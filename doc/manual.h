/**********************************************************************************
 * Copyright (c) 2020 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * @file manual.h
 *
 * @brief File containing the front page of the MAiNGO documentation.
 *
 **********************************************************************************/

#pragma once


/**

@mainpage MeLOn - Machine Learning models for Optimization
@authors Artur M. Schweidtmann, Linus Netze, and Alexander Mitsos
@date 26.05.2020

Thank you for using MeLOn! If you have any issues, concerns, or comments, please communicate them using the <a>Issues</a> functionality at https://git.rwth-aachen.de/avt.svt/public/melon.git or send an e-mail to artur.schweidtmann@rwth-aachen.de

@section intro_sec Introduction

MeLOn provides scripts for the training of various machine-learning models and their C++ implementation which can be used in the open-source solver MAiNGO.  The machine-learning module git repository currently contains the following models:
* Artificial neural networks for regression
* Gaussian processes for regression (also known as Kriging)

Further models are under current develpment and will be published soon.

Note that MeLOn is a submodule of our global solver MAiNGO. Thus, it will be automatically included as a dependency when you download MAiNGO and initialize & update the submodules. Then, the developed machine learning models can be used within MAiNGO.

Also, note that we have developed various envelopes or tight convex and concave relaxatoins of relevant functions. These functions are implemented within Mc++. When using MeLOn or MAiNGO, Mc++ is automatically inlcuded as a submodule.

This manual is divided in the following sections:
- \subpage install
- \subpage bib

\page install Installing and Executing MeLOn

When you want to use machine-learning models in optimization, we recommend to you to download and install the MAiNGO solver.
We provide MeLOn as a submodule within MAiNGO. This way, the machine-learning models can be directly used within optimization problems.
If you want to see a few examples for the usage of machine learning models in MAiNGO, please see the example problems here: https://git.rwth-aachen.de/avt.svt/public/maingo/-/tree/master/examples 
As described below, you can also download MeLOn as a standalone library for machine learning models in C++.
When compiling MeLOn as a standalone tool, we automatically set up a few tests.

@section get_melon Obtaining MeLOn

In order to obtain MeLOn, we strongly recommend using Git (Git Bash on Windows). It is possible to download MeLOn as a .zip folder, but we strongly recommend to
use Git and follow the given instructions (in particular because we use submodules for dependencies, which need to be downloaded separately when not using Git). First you need to clone the MeLOn repository. This is done by
\code{.sh}
git clone https://git.rwth-aachen.de/avt.svt/public/melon.git <directory>
\endcode
where \<directory\> is the name of the directory where you can find all MeLOn files after cloning (default name is melon). After the cloning is done, simply navigate to the directory. In the
cloned MeLOn directory execute
\code{.sh}
git submodule init
\endcode
If nothing happens, you are <b>not</b> in the cloned MeLOn directory and have to navigate to there first.
For Windows users, we recommend the usage of HTTPS protocols for Git and SSH protocols for Linux and Mac OS users to avoid multiple username and password inputs.
For more info on SSH keys, see the Git documentation on SSH authentication which can be found under <b>Help -> SSH authentication</b> in your GitLab.
If you are having HTTPS authentication problems on Windows, please make sure that your cedential manager has the correct settings. The Windows credential manager can be found at
<b>Control Panel -> User Accounts -> Manage your credentials</b>.

In order to proceed and update all submodules of MeLOn, execute
\code{.sh}
git submodule update -j 1
\endcode
in the MeLOn directory. You can determine whether the above worked properly by checking if each dependency folder in the <tt>dep/</tt> folder is non-empty.
In the case that the above <b>\e DID \e NOT</b> work properly, execute the
following in the MeLOn directory
\code{.sh}
git submodule foreach git checkout master
git submodule foreach git pull
\endcode
A note for user more familiar with Git: the <tt>git submodule update</tt> is exectued without the <tt>--recursive</tt> option. This avoids instantiating any indirect dependencies; in the repository
design of MeLOn, all dependencies (including indirect ones) are present in the <tt>dep</tt> folder. It is also executed using only one process <tt>-j 1</tt> to avoid input failures.

@subsection update_melon Updating MeLOn

If you want to update to the latest MeLOn version, use the following git commands
\code{.sh}
git pull
git submodule update
\endcode
If you changed any of the source files in MeLOn or any dependency found in the <tt>dep/</tt> folder, you should restore the original state of MeLOn by first saving a copy of the files you changed (or using <tt>git stash</tt>) and then executing
\code{.sh}
git checkout .
\endcode
in the MeLOn repository and/or in any dependency repository you edited. Then, update the MeLOn repository with the `pull` and `submodule update` commands. Finally, you can replace the files of interest by your saved copies (or retrieve them via <tt>git stash pop</tt>).


@section req_software Required Software

Building MeLOn requires the following non-standard programs that are not in the Git repository:
- CMake 3.8 or later
- Visual Studio 2017 (Windows only)
- A Fortran Compiler (Linux and Mac OS)

All other third-party software that MeLOn depends on comes with the MeLOn Git. Unless you feel the need to modify the folder structure or switch to other versions (in which case we would
appreciate you contacting the MeLOn team as well), there is nothing you need to do.

@section cmake Generating and Compiling the Project

MeLOn uses CMake for setting up the required Visual Studio project (for Windows) or Makefile (for Linux or Mac OS).

A note for users seeking to include MeLOn in their own code: MeLOn uses modern target-oriented cmake commands. The <tt>CMakeLists.txt</tt> in the root directory is the sole entry point both for building MeLOn as a standalone solver or including
it into your project. However, when including it into your code you will need to add all dependencies (i.e., all folders within the <tt>dep</tt> folder in the MeLOn repository) in your own CMakeLists.txt using <tt>add_subdirectory</tt>.
Please see also section \ref embedded.

@subsection cmake_win Windows

On Windows, only Microsoft Visual C++ 2017 is supported. We supply pre-compiled versions for all Fortran libraries, so no Fortran compiler (or runtime) should be needed.
To generate the Visual Studio project and compile MeLOn, you need to complete the following steps:

1. Start CMake and navigate or type the path to your MeLOn directory (this is the one where the Readme.md is) and select your build directory.
\htmlonly <style>div.image img[src="CMakeHead.PNG"]{width:15cm;align:left}</style> \endhtmlonly
\image html CMakeHead.PNG width=15cm

2. Use the \e Configure button to choose Visual Studio 15 2017 Win64 as generator (or Visual Studio 15 2017 as generator and x64 as optional platform for generator in the newer versions of CMake).
Make sure that you use default native compilers. Press \e Finish and wait for the configuration to complete. If at the end you get a message saying <tt>Configuring done</tt>, everything worked fine.
\htmlonly <style>div.image img[src="CMakeGenerator.PNG"]{width:25cm;align:left}</style> \endhtmlonly
\image html CMakeGenerator.PNG width=25cm

3. If desired, you can now change the CMake variables. (not applicable here)

4. Press the \e Generate button. You should get a message saying <tt>Generating done</tt>.

5. Press the \e Open \e Project button (or open the <tt>MeLOn.sln</tt> file (with Visual Studio 15 2017) that was created by CMake in the build directory you specified).
Make sure to set the build type to \e Release, since this will result in MeLOn being significantly faster.
\htmlonly <style>div.image img[src="VS_Release.PNG"]{width:6cm;align:left}</style> \endhtmlonly
\image html VS_Release.PNG width=6cm

6. Compile MeLOn by clicking <tt>Build->Build solution</tt>. This will create the executables.

@subsection cmake_linux_os Linux and Mac OS

We recommend to create a build folder in the MeLOn directory first and then navigate to it (you can create the build folder anywhere else on you machine).
\code{.sh}
mkdir build
cd build
\endcode
Then simply execute cmake using the CMakeLists.txt from the MeLOn directory by
\code{.sh}
cmake ..
\endcode
You can change the CMake variables explained above by adding <tt>-D\<name_of_cmake_variable\>=\<value\></tt> after the <tt>cmake</tt> command. 
To compile the code execute
\code{.sh}
make
\endcode
You can add the option <tt>-j n</tt> to compile using n cores of your machine, e.g., execute <tt>make -j 4</tt> to compile using 4 cores.


\page bib Where can I read more?
    
@section readUses Uses of MAiNGO
Examples of applications of earlier versions of MeLOn can be found in:
	- W.R. Huster, A.M. Schweidtmann and A. Mitsos, Impact of accurate working fluid properties on the globally optimal design of an organic Rankine cycle, Computer Aided Chemical Engineering 47 (2019) 427-432.
	- W.R. Huster, A.M. Schweidtmann and A. Mitsos, Working fluid selection for organic rankine cycles via deterministic global optimization of design and operation, Optimization and Engineering (2019) in press.
    - D. Rall, D. Menne, A.M. Schweidtmann, J. Kamp, L. von Kolzenberg, A. Mitsos and Matthias Wessling, Rational design of ion separation membranes, Journal of Membrane Science 569 (2019) 209-219.
	- D. Rall, A.M. Schweidtmann, B., Aumeier, J. Kamp, J. Karwe, K., Ostendorf, K., A. Mitsos & Wessling, M. (2020). Simultaneous rational design of ion separation membranes and processes. Journal of Membrane Science, 600, 117860.
	- D. Rall, A.M. Schweidtmann, M. Kruse, E., Evdochenko, A., Mitsos, A., & Wessling, M. (2020). Multi-scale membrane process optimization with high-fidelity ion transport models through machine learning. Journal of Membrane Science, 118208.
	- P. Schäfer, A.M. Schweidtmann, P.H.A. Lenz, H.M.C. Markgraf, A. Mitsos, Wavelet-based grid-adaptation for nonlinear scheduling subject to time-variable electricity prices, Computers and Chemical Engineering (2019) in press.
	- A.M. Schweidtmann, D. Bongartz, W.R. Huster, A. Mitsos, Deterministic Global Process Optimization: Flash Calculations via Artificial Neural Networks, Computer Aided Chemical Engineering 46 (2019) 937-942.
	- A.M. Schweidtmann and A. Mitsos, Deterministic Global Optimization with Artificial Neural Networks Embedded, Journal of Optimization Theory and Applications 180 (2019) 925-948.
	- A.M. Schweidtmann, W.R. Huster, J.T. Lüthje and A. Mitsos, Deterministic global process optimization: Accurate (single-species) properties via artificial neural networks, Computers & Chemical Engineering 121 (2019) 67-74.
*/
