/**
 \page mulfil-gp Multifidelity Gaussian process

 @section mulfil-gp-general General information
 The implementation of multifidelity Gaussian processes in MeLOn refers to 'DOI: 10.1615/Int.J.UncertaintyQuantification.2014006914'.
 Currently only two levels of fidelity (low and high fidelity), a constant rho and no noise are supported.
 The input sets for low and high fidelity are expected to be nested.

 @section mulfil-gp-training Training
 MeLOn supports one option for training multifidelity Gaussian processes using Python with the emukit toolbox.
 It is recommended that you rely on the provided example scripts for training your model. The reason for this is that MeLOn doesn't support all functionality included in emukit and by using the examples you can assure to generate models which are compatible with the MeLOn functionalities.
 Requirements
- Python (>=3.9)
- emukit (>=0.4.11)

 @section mulfil-gp-examples Example scripts
 There are two Python example scripts
- The script `example_mulfilgp_training.py` trains an emukit model and creates .json files that can be read by MeLOn.
- The script `example_mulfilgp_maingopy.py` creates a MeLOn MulfilGp object from .json files and uses its prediction and variance functions to create an optimization problem that is solved with maingopy.

 */