/**
\page models Models

The MeLOn library currently includes the machine learning models listed below. Their respective pages provide you with information on how to train and use them in your MAiNGO problem formulation:
- \subpage ffnet
- \subpage gp
- \subpage mulfil-gp
- \subpage svm
*/
