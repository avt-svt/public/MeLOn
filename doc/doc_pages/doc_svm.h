/**
\page svm Support vector machines 

@section svm-training Training

MeLOn supports two different types of support vector machines:
- support vector regression
- one class support vector machine

Both model types can be trained using python with the Scikit-Learn package and example training scripts are provided for each. These examples should act as a guide to setting up your own training scripts.  
__Notice:__ Because not all functionalities that are included in Scikit-learn are supported by MeLOn you should refer to the supported features and their restrictions in the following section when you modify the example training scripts or when you set up your own.

@subsection svm-training-requirements Requirements
- Python (>=3.6)
- scikit-learn (>=0.23.2)
- numpy (>=1.19.4)
- matplotlib (>=3.3.3)
- pandas (>=1.14)

@subsection svm-training-model Model
For information on the different model settings and parameters and how to use and set them, please refer to the Scikit-learn documentation of the respective model([Support vector regression](https://sklearn.org/modules/generated/sklearn.svm.SVR.html), [One class support vector machine](https://scikit-learn.org/stable/modules/generated/sklearn.svm.OneClassSVM.html)).

@subsubsection svm-training-model-restrictions Restrictions
There is only one restriction for choosing the model parameters. Only the following kernels are supported by MeLOn:
- linear (`linear`) 
- radial basis function (`rbf`)

@subsection svm-training-scaling Feature scaling
MeLOn supports the use of in- and output feature scaling using either a [MinMax-](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html) or a [Standard-scaler](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html).

@subsubsection svm-training-scaling-restrictions Restrictions
Currently, only a feature range of [-1,1] is supported for the MinMax-scaling, therefore you have to pass `feature_range=(-1,1)` as an argument to the constructor of the MinMax-Scaler.


@subsection svm-training-file Generating the model file.
To generate the json model file used by the C++ part of MeLOn you have to use the `save_model_to_json` function from `utils.py`.  
Parameters of `save_model_to_json`:
- `filepath`: Path to the location to which the file should be saved
- `filename`: Name of the file
- `clf`: Model object
- `scalers` (optional): Dictionary that contains the feature scaler object. Has to have the following form:
   \code
   {
      'input': input_scaler_object,
      'output': output_scaler_object
   }
   \endcode
   In case you only want to have either in- or output scaling you can leave the other out of the dictionary.



@section svm-usage Usage
To include the trained network in your MAiNGO problem formulation you can use the MeLOn C++ interface. An example of how to include support vector machines in your optimization can be found in the MAiNGO folder.

@subsection svm-usage-constructors Constructors

SupportVectorRegression and SupportVectorMachineOneClass inherit the constructors of SupportVectorMachine, therefore refer to its documentation for a description of the constructors.

@subsubsection svm-usage-constructors-svm Support vector machine (Abstract parent class: Can't be called!)
1. melon::SupportVectorMachine<T>::SupportVectorMachine()
2. melon::SupportVectorMachine<T>::SupportVectorMachine(std::string modelName)
3. melon::SupportVectorMachine<T>::SupportVectorMachine(std::string modelPath, std::string modelName)
4. melon::SupportVectorMachine<T>::SupportVectorMachine(std::shared_ptr<SvmData> modelData)

@subsubsection svm-usage-constructors-svr Support vector regression
1. melon::SupportVectorRegression<T>::SupportVectorRegression()
2. melon::SupportVectorRegression<T>::SupportVectorRegression(std::string modelName)
3. melon::SupportVectorRegression<T>::SupportVectorRegression(std::string modelPath, std::string modelName)
4. melon::SupportVectorRegression<T>::SupportVectorRegression(std::shared_ptr<SvmData> modelData)

@subsubsection svm-usage-constructors-ocsvm One class support vector machine
1. melon::SupportVectorMachineOneClass<T>::SupportVectorMachineOneClass()
2. melon::SupportVectorMachineOneClass<T>::SupportVectorMachineOneClass(std::string modelName)
3. melon::SupportVectorMachineOneClass<T>::SupportVectorMachineOneClass(std::string modelPath, std::string modelName, MODEL_FILE_TYPE fileType)
4. melon::SupportVectorMachineOneClass<T>::SupportVectorMachineOneClass(std::shared_ptr<SvmData> modelData)

@subsection svm-usage-prediction Prediction

@subsubsection svm-usage-prediction-svr Support vector regression
1. melon::SupportVectorRegression<T>::calculate_prediction_reduced_space(const std::vector<T> input)
2. melon::SupportVectorRegression<T>::calculate_prediction_full_space(const std::vector<T> input, const std::vector<T> internalVariables, std::vector<T>& constraints)

@subsubsection svm-usage-prediction-ocsvm One class support vector machine
1. melon::SupportVectorMachineOneClass<T>::calculate_prediction_reduced_space(const std::vector<T> input)
2. melon::SupportVectorMachineOneClass<T>::calculate_prediction_full_space(const std::vector<T> input, const std::vector<T> internalVariables, std::vector<T>& constraints)

@subsection svm-usage-internal-variables Internal variables

@subsubsection svm-usage-internal-variables-svr Support vector regression
1.  melon::SupportVectorRegression<T>::get_number_of_full_space_variables()
2.  melon::SupportVectorRegression<T>::get_fullspace_variables(size_t& variableNumber, std::vector<std::string>& variableNames, std::vector<std::pair<double, double>>& variableBounds)

@subsubsection svm-usage-internal-variables-ocsvm One class support vector machine
1.  melon::SupportVectorMachineOneClass<T>::get_number_of_full_space_variables()
2.  melon::SupportVectorMachineOneClass<T>::get_fullspace_variables(size_t& variableNumber, std::vector<std::string>& variableNames, std::vector<std::pair<double, double>>& variableBounds)

*/