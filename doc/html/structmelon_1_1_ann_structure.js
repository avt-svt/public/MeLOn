var structmelon_1_1_ann_structure =
[
    [ "activationFunction", "structmelon_1_1_ann_structure.html#a105f394f328ec4db9f3b04a42fa620d8", null ],
    [ "biasConnect", "structmelon_1_1_ann_structure.html#a53f444127c72cd4974588d8fe5bde250", null ],
    [ "inputConnect", "structmelon_1_1_ann_structure.html#aac68a367dad6322936825ea88bcc5a68", null ],
    [ "inputSize", "structmelon_1_1_ann_structure.html#a1e7a81af797e4f9aa27e70f47c8735e8", null ],
    [ "layerConnect", "structmelon_1_1_ann_structure.html#ac38f895d9a1732edfbb473613b20b504", null ],
    [ "layerSize", "structmelon_1_1_ann_structure.html#a884e5315a0f182176426a8be18436715", null ],
    [ "normalizedOutput", "structmelon_1_1_ann_structure.html#a9506ef28c29e149fe0283ad7311fa275", null ],
    [ "numLayers", "structmelon_1_1_ann_structure.html#a6a3a954be4652ddccc8790fca4830a3b", null ],
    [ "scaledInput", "structmelon_1_1_ann_structure.html#aa74231a71fc4e0d1380b441ece0b527c", null ]
];