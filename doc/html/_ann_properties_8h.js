var _ann_properties_8h =
[
    [ "melon::AnnStructure", "structmelon_1_1_ann_structure.html", "structmelon_1_1_ann_structure" ],
    [ "melon::AnnWeights", "structmelon_1_1_ann_weights.html", "structmelon_1_1_ann_weights" ],
    [ "melon::AnnNormalizationParameters", "structmelon_1_1_ann_normalization_parameters.html", "structmelon_1_1_ann_normalization_parameters" ],
    [ "melon::AnnData", "structmelon_1_1_ann_data.html", "structmelon_1_1_ann_data" ],
    [ "ACTIVATION_FUNCTION", "_ann_properties_8h.html#a48f12885945641a41597e6668940dc73", [
      [ "PURE_LIN", "_ann_properties_8h.html#a48f12885945641a41597e6668940dc73a2f195b9787f598a7ee661a881c474e3b", null ],
      [ "TANH", "_ann_properties_8h.html#a48f12885945641a41597e6668940dc73ad3d7d3b5435748e6d590a93d042de6c2", null ],
      [ "RELU", "_ann_properties_8h.html#a48f12885945641a41597e6668940dc73acb18be48f9330d10bfd46e4046f09d3c", null ],
      [ "RELU6", "_ann_properties_8h.html#a48f12885945641a41597e6668940dc73a48a94bc178c62695a3d5142cb8b984ed", null ]
    ] ]
];