var structmelon_1_1_g_p_data =
[
    [ "DX", "structmelon_1_1_g_p_data.html#a9747c4e9b1dc647a48ca708254d141f0", null ],
    [ "DY", "structmelon_1_1_g_p_data.html#abc4059f55f790dd99b89f06edc280807", null ],
    [ "inputScalerData", "structmelon_1_1_g_p_data.html#ab4efbfa86dbc7ca4cbd863a3629c5ad5", null ],
    [ "invK", "structmelon_1_1_g_p_data.html#a4bf3b1c58e1c2d09ab695d564db4bd89", null ],
    [ "K", "structmelon_1_1_g_p_data.html#af5a6789b5fc9709e3da7ff6a3ab8d417", null ],
    [ "kernelData", "structmelon_1_1_g_p_data.html#abf55d00a79969bcb9ddcb74575b6a5ab", null ],
    [ "matern", "structmelon_1_1_g_p_data.html#a38be65d3ef7888bde2f9c2aaa26a296d", null ],
    [ "meanfunction", "structmelon_1_1_g_p_data.html#a190192bb9474c24510e8b34e3fae0763", null ],
    [ "nX", "structmelon_1_1_g_p_data.html#af94460988623f8f8f5debb885629ed58", null ],
    [ "predictionScalerData", "structmelon_1_1_g_p_data.html#a0016bdf5ba5ce879a011eea2f15e6149", null ],
    [ "stdOfOutput", "structmelon_1_1_g_p_data.html#a545f45a74992594c1abab224da943152", null ],
    [ "X", "structmelon_1_1_g_p_data.html#a9bf51f6c5f0c65ba06eeba3c16e18796", null ],
    [ "Y", "structmelon_1_1_g_p_data.html#ac2ca3a861eef23a4aab4d64009bc6aec", null ]
];