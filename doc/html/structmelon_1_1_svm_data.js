var structmelon_1_1_svm_data =
[
    [ "dualCoefficients", "structmelon_1_1_svm_data.html#a02125fa378b434a59489fe68867a9c54", null ],
    [ "inputScalerData", "structmelon_1_1_svm_data.html#aa59d2ea919c99ce32643810d96be322c", null ],
    [ "kernelFunction", "structmelon_1_1_svm_data.html#a63b5cf742541499e1f7cb0a3bae170bd", null ],
    [ "kernelParameters", "structmelon_1_1_svm_data.html#a4584851c196fdb3a1f53ea734cb24ca7", null ],
    [ "outputScalerData", "structmelon_1_1_svm_data.html#abd271036f149a4d666c6a52c5798bd22", null ],
    [ "rho", "structmelon_1_1_svm_data.html#a91b7f46819897d76e7b3d69352f8bcad", null ],
    [ "supportVectors", "structmelon_1_1_svm_data.html#a8701eee707298b7fe840bea845e8912e", null ]
];