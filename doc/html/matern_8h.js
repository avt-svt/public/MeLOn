var matern_8h =
[
    [ "melon::kernel::KernelData", "structmelon_1_1kernel_1_1_kernel_data.html", "structmelon_1_1kernel_1_1_kernel_data" ],
    [ "melon::kernel::Matern< T, V >", "classmelon_1_1kernel_1_1_matern.html", "classmelon_1_1kernel_1_1_matern" ],
    [ "melon::kernel::Matern12< T, V >", "classmelon_1_1kernel_1_1_matern12.html", "classmelon_1_1kernel_1_1_matern12" ],
    [ "melon::kernel::Matern32< T, V >", "classmelon_1_1kernel_1_1_matern32.html", "classmelon_1_1kernel_1_1_matern32" ],
    [ "melon::kernel::Matern52< T, V >", "classmelon_1_1kernel_1_1_matern52.html", "classmelon_1_1kernel_1_1_matern52" ],
    [ "melon::kernel::MaternInf< T, V >", "classmelon_1_1kernel_1_1_matern_inf.html", "classmelon_1_1kernel_1_1_matern_inf" ],
    [ "USE_TAYLORMADE_RELAXATIONS", "matern_8h.html#aee89c193f8f9fd486ebeecd9604daa6f", null ],
    [ "covar_matern_1", "matern_8h.html#af325146267524e3bec94d0535ffcebcb", null ],
    [ "covar_matern_3", "matern_8h.html#af6326be288cac254a67493632f4ddf76", null ],
    [ "covar_matern_5", "matern_8h.html#a1805b33bc8adf981fa654aaf8ff1fae7", null ],
    [ "covar_sqrexp", "matern_8h.html#a2a227d1955df0c4b01af2c41b9470e95", null ]
];