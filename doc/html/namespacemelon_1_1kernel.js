var namespacemelon_1_1kernel =
[
    [ "Kernel", "classmelon_1_1kernel_1_1_kernel.html", "classmelon_1_1kernel_1_1_kernel" ],
    [ "KernelCompositeAdd", "classmelon_1_1kernel_1_1_kernel_composite_add.html", "classmelon_1_1kernel_1_1_kernel_composite_add" ],
    [ "KernelCompositeMultiply", "classmelon_1_1kernel_1_1_kernel_composite_multiply.html", "classmelon_1_1kernel_1_1_kernel_composite_multiply" ],
    [ "KernelConstant", "classmelon_1_1kernel_1_1_kernel_constant.html", "classmelon_1_1kernel_1_1_kernel_constant" ],
    [ "KernelData", "structmelon_1_1kernel_1_1_kernel_data.html", "structmelon_1_1kernel_1_1_kernel_data" ],
    [ "KernelRBF", "classmelon_1_1kernel_1_1_kernel_r_b_f.html", "classmelon_1_1kernel_1_1_kernel_r_b_f" ],
    [ "Matern", "classmelon_1_1kernel_1_1_matern.html", "classmelon_1_1kernel_1_1_matern" ],
    [ "Matern12", "classmelon_1_1kernel_1_1_matern12.html", "classmelon_1_1kernel_1_1_matern12" ],
    [ "Matern32", "classmelon_1_1kernel_1_1_matern32.html", "classmelon_1_1kernel_1_1_matern32" ],
    [ "Matern52", "classmelon_1_1kernel_1_1_matern52.html", "classmelon_1_1kernel_1_1_matern52" ],
    [ "MaternInf", "classmelon_1_1kernel_1_1_matern_inf.html", "classmelon_1_1kernel_1_1_matern_inf" ],
    [ "StationaryKernel", "classmelon_1_1kernel_1_1_stationary_kernel.html", "classmelon_1_1kernel_1_1_stationary_kernel" ],
    [ "covar_matern_1", "namespacemelon_1_1kernel.html#af325146267524e3bec94d0535ffcebcb", null ],
    [ "covar_matern_3", "namespacemelon_1_1kernel.html#af6326be288cac254a67493632f4ddf76", null ],
    [ "covar_matern_5", "namespacemelon_1_1kernel.html#a1805b33bc8adf981fa654aaf8ff1fae7", null ],
    [ "covar_sqrexp", "namespacemelon_1_1kernel.html#a2a227d1955df0c4b01af2c41b9470e95", null ]
];