var searchData=
[
  ['rbf_0',['RBF',['../namespacemelon.html#ae9d1641da9e08a0d25d8d6cefcc92887ae0c0181a2948647ea58c0940cce1b47e',1,'melon']]],
  ['read_20more_1',['Where can I read more?',['../bib.html',1,'index']]],
  ['readbom_2',['ReadBOM',['../classtinyxml2_1_1_x_m_l_util.html#ae9bcb2bc3cd6475fdc644c8c17790555',1,'tinyxml2::XMLUtil']]],
  ['regression_3',['Regression',['../svm.html#svm-usage-constructors-svr',1,'Support vector regression'],['../svm.html#svm-usage-prediction-svr',1,'Support vector regression'],['../svm.html#svm-usage-internal-variables-svr',1,'Support vector regression']]],
  ['relu_4',['RELU',['../namespacemelon.html#a48f12885945641a41597e6668940dc73acb18be48f9330d10bfd46e4046f09d3c',1,'melon']]],
  ['relu6_5',['RELU6',['../namespacemelon.html#a48f12885945641a41597e6668940dc73a48a94bc178c62695a3d5142cb8b984ed',1,'melon']]],
  ['required_20software_6',['Required Software',['../install.html#req_software',1,'']]],
  ['requirements_7',['Requirements',['../ffnet.html#ffnet-python-requirements',1,'Requirements'],['../gp.html#gp-python-training-requirements',1,'Requirements'],['../svm.html#svm-training-requirements',1,'Requirements']]],
  ['reset_8',['Reset',['../classtinyxml2_1_1_str_pair.html#a80c1b3bd99bf62ae85c94a29ce537125',1,'tinyxml2::StrPair']]],
  ['restrictions_9',['Restrictions',['../svm.html#svm-training-model-restrictions',1,'Restrictions'],['../svm.html#svm-training-scaling-restrictions',1,'Restrictions']]],
  ['ret_10',['RET',['../classmelon_1_1kernel_1_1_kernel.html#afc65772b466950a64138b8914d44a0b1',1,'melon::kernel::Kernel']]],
  ['rho_11',['rho',['../structmelon_1_1_mulfil_gp_data.html#aa432863b3c01d6e2fd3f4fdb2b53da20',1,'melon::MulfilGpData::rho'],['../structmelon_1_1_svm_data.html#a91b7f46819897d76e7b3d69352f8bcad',1,'melon::SvmData::rho']]],
  ['rootelement_12',['RootElement',['../classtinyxml2_1_1_x_m_l_document.html#a0e6855771cbe87d839fb301d3646f5b8',1,'tinyxml2::XMLDocument::RootElement()'],['../classtinyxml2_1_1_x_m_l_document.html#a52ea1ace64749d97cf61bf9537fb99ec',1,'tinyxml2::XMLDocument::RootElement() const']]]
];
