var searchData=
[
  ['be_20called_0',['Support vector machine (Abstract parent class: Can&apos;t be called!)',['../svm.html#svm-usage-constructors-svm',1,'']]],
  ['biasconnect_1',['biasConnect',['../structmelon_1_1_ann_structure.html#a53f444127c72cd4974588d8fe5bde250',1,'melon::AnnStructure']]],
  ['biasweight_2',['biasWeight',['../structmelon_1_1_ann_weights.html#ae8127c6eb68e821eb3df52150a179539',1,'melon::AnnWeights']]],
  ['block_3',['Block',['../structtinyxml2_1_1_mem_pool_t_1_1_block.html',1,'tinyxml2::MemPoolT']]],
  ['boolattribute_4',['BoolAttribute',['../classtinyxml2_1_1_x_m_l_element.html#a53eda26131e1ad1031ef8ec8adb51bd8',1,'tinyxml2::XMLElement']]],
  ['booltext_5',['BoolText',['../classtinyxml2_1_1_x_m_l_element.html#a68569f59f6382bcea7f5013ec59736d2',1,'tinyxml2::XMLElement']]],
  ['boolvalue_6',['BoolValue',['../classtinyxml2_1_1_x_m_l_attribute.html#a98ce5207344ad33a265b0422addae1ff',1,'tinyxml2::XMLAttribute']]],
  ['buf_5fsize_7',['BUF_SIZE',['../classtinyxml2_1_1_x_m_l_attribute.html#a1543d5687af193553e0803804c01f377a5c77cc230dc9e6f9011ba6baa5cf6aaa',1,'tinyxml2::XMLAttribute::BUF_SIZE'],['../classtinyxml2_1_1_x_m_l_element.html#a07a6ce25c17aaa505933db57f2373e50a21625cc023a5f905272d3f116cb9143e',1,'tinyxml2::XMLElement::BUF_SIZE'],['../classtinyxml2_1_1_x_m_l_printer.html#ac9049bee10d230eb31ff7d146d538f7aa1f747a8fb39ceb2e711c3798058fb632',1,'tinyxml2::XMLPrinter::BUF_SIZE']]]
];
