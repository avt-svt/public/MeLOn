var searchData=
[
  ['y_1591',['Y',['../structmelon_1_1_g_p_data.html#ac2ca3a861eef23a4aab4d64009bc6aec',1,'melon::GPData::Y()'],['../example__training__of___a_n_n_8m.html#ab5d0dee4577dce4166b4f7d03510a14e',1,'Y():&#160;example_training_of_ANN.m'],['../example__training__of___g_p_8m.html#ab5d0dee4577dce4166b4f7d03510a14e',1,'Y():&#160;example_training_of_GP.m'],['../_write___g_p__to__json_8m.html#a2f7178b5691c597aba4e7ff9eaf0524d',1,'Y():&#160;Write_GP_to_json.m']]],
  ['y_5fprediction_1592',['y_prediction',['../example__training__of___a_n_n_8m.html#a8e137b3fb5c279332b8f4c38faa5cd81',1,'y_prediction():&#160;example_training_of_ANN.m'],['../example__training__of___g_p_8m.html#a8e137b3fb5c279332b8f4c38faa5cd81',1,'y_prediction():&#160;example_training_of_GP.m']]],
  ['y_5fstd_1593',['y_std',['../example__training__of___a_n_n_8m.html#adf359d7e263aa0342dc5a4b7e9940381',1,'y_std():&#160;example_training_of_ANN.m'],['../example__training__of___g_p_8m.html#adf359d7e263aa0342dc5a4b7e9940381',1,'y_std():&#160;example_training_of_GP.m']]],
  ['ynew_1594',['Ynew',['../_scale_variables_8m.html#ad6666ab10465ac9fde34c0543948cfb8',1,'ScaleVariables.m']]],
  ['yselement_1595',['YSElement',['../_write___g_p__to__xml_8m.html#a1540d6a5c379f2b85bb00ab3c348009b',1,'Write_GP_to_xml.m']]],
  ['ysnode_1596',['YSNode',['../_write___g_p__to__xml_8m.html#acea103cf4f7f5a6b750b17d5b65f64ef',1,'Write_GP_to_xml.m']]]
];
