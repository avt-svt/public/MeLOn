var searchData=
[
  ['x_1584',['X',['../structmelon_1_1_g_p_data.html#a9bf51f6c5f0c65ba06eeba3c16e18796',1,'melon::GPData::X()'],['../_write___g_p__to__json_8m.html#a4817a9277ef40b3369c95a4e1e1acfaf',1,'X():&#160;Write_GP_to_json.m']]],
  ['x2_1585',['X2',['../_predict___g_p_8m.html#a0e3b80f846c2d80a70b3a1a74701b9f4',1,'Predict_GP.m']]],
  ['x_5f2_5fprediction_1586',['x_2_prediction',['../example__training__of___a_n_n_8m.html#a5d4ceb95920472829ed9a1f644d4c9de',1,'x_2_prediction():&#160;example_training_of_ANN.m'],['../example__training__of___g_p_8m.html#a5d4ceb95920472829ed9a1f644d4c9de',1,'x_2_prediction():&#160;example_training_of_GP.m']]],
  ['x_5ftest_5fscaled_1587',['x_test_scaled',['../_predict___g_p_8m.html#a771f0da6537af3e2b716e8671e948f56',1,'Predict_GP.m']]],
  ['xmlfilename_1588',['xmlFileName',['../_write___g_p__to__xml_8m.html#a597970326d80c9e00d8d6f09d27c9c78',1,'Write_GP_to_xml.m']]],
  ['xselement_1589',['XSElement',['../_write___g_p__to__xml_8m.html#a9338dace0083d876a22a190b808ff3b8',1,'Write_GP_to_xml.m']]],
  ['xsnode_1590',['XSNode',['../_write___g_p__to__xml_8m.html#af501138ec7abe2b7a1455ced178ba447',1,'Write_GP_to_xml.m']]]
];
