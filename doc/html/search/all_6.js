var searchData=
[
  ['feature_20scaling_0',['Feature scaling',['../svm.html#svm-training-scaling',1,'']]],
  ['feed_20forward_20neural_20network_1',['Feed forward neural network',['../ffnet.html',1,'models']]],
  ['feedforwardnet_2',['FeedForwardNet',['../classmelon_1_1_feed_forward_net.html',1,'melon::FeedForwardNet&lt; T &gt;'],['../classmelon_1_1_feed_forward_net.html#a40d5999e0803420f02e2285aee4d7f19',1,'melon::FeedForwardNet::FeedForwardNet()'],['../classmelon_1_1_feed_forward_net.html#ac1ac26b7d389470fc0a13463cddd9a3f',1,'melon::FeedForwardNet::FeedForwardNet(std::string modelName, MODEL_FILE_TYPE fileType)'],['../classmelon_1_1_feed_forward_net.html#aa5bfb8592ff40db349c0699f22dcaf6e',1,'melon::FeedForwardNet::FeedForwardNet(std::string modelPath, std::string modelName, MODEL_FILE_TYPE fileType)'],['../classmelon_1_1_feed_forward_net.html#a5aa1638cb722f3139dd95f229d0ba0a2',1,'melon::FeedForwardNet::FeedForwardNet(std::shared_ptr&lt; AnnData &gt; modelData)']]],
  ['ffnet_2eh_3',['ffNet.h',['../ff_net_8h.html',1,'']]],
  ['file_4',['Generating the model file.',['../svm.html#svm-training-file',1,'']]],
  ['findattribute_5',['FindAttribute',['../classtinyxml2_1_1_x_m_l_element.html#a157750dac8037a316fd1af1a973dfa2c',1,'tinyxml2::XMLElement']]],
  ['findorcreateattribute_6',['FindOrCreateAttribute',['../classtinyxml2_1_1_x_m_l_element.html#ac9d8fc849ec8acf46678217de011e896',1,'tinyxml2::XMLElement']]],
  ['firstattribute_7',['FirstAttribute',['../classtinyxml2_1_1_x_m_l_element.html#a4a7c4392658833bb8138c474d15a805a',1,'tinyxml2::XMLElement']]],
  ['firstchild_8',['FirstChild',['../classtinyxml2_1_1_x_m_l_node.html#abcc2e572dce329539689b426a0840661',1,'tinyxml2::XMLNode::FirstChild() const'],['../classtinyxml2_1_1_x_m_l_node.html#a3cc3c85d40ce73d6f5849a35debfccfc',1,'tinyxml2::XMLNode::FirstChild()'],['../classtinyxml2_1_1_x_m_l_handle.html#a536447dc7f54c0cd11e031dad94795ae',1,'tinyxml2::XMLHandle::FirstChild()'],['../classtinyxml2_1_1_x_m_l_const_handle.html#aef06bd16cb308652a32b864b0a743136',1,'tinyxml2::XMLConstHandle::FirstChild()']]],
  ['firstchildelement_9',['FirstChildElement',['../classtinyxml2_1_1_x_m_l_node.html#a1bec132dcf085284e0a10755f2cf0d57',1,'tinyxml2::XMLNode::FirstChildElement(const char *name=0) const'],['../classtinyxml2_1_1_x_m_l_node.html#a46c460e1494ccc2691f515a34e78abc0',1,'tinyxml2::XMLNode::FirstChildElement(const char *name=0)'],['../classtinyxml2_1_1_x_m_l_handle.html#a74b04dd0f15e0bf01860e282b840b6a3',1,'tinyxml2::XMLHandle::FirstChildElement()'],['../classtinyxml2_1_1_x_m_l_const_handle.html#ac747db472ffc55c5af2e82ffec813640',1,'tinyxml2::XMLConstHandle::FirstChildElement()']]],
  ['fits_10',['Fits',['../structtinyxml2_1_1_long_fits_into_size_t_minus_one.html#a3057710104ab733963eb32fda0bc374c',1,'tinyxml2::LongFitsIntoSizeTMinusOne::Fits()'],['../structtinyxml2_1_1_long_fits_into_size_t_minus_one_3_01false_01_4.html#a29b01087f38a951276df69d358dc0764',1,'tinyxml2::LongFitsIntoSizeTMinusOne&lt; false &gt;::Fits()']]],
  ['floatattribute_11',['FloatAttribute',['../classtinyxml2_1_1_x_m_l_element.html#ab1f4be2332e27dc640e9b6abd01d64dd',1,'tinyxml2::XMLElement']]],
  ['floattext_12',['FloatText',['../classtinyxml2_1_1_x_m_l_element.html#a45444eb21f99ca46101545992dc2e927',1,'tinyxml2::XMLElement']]],
  ['floatvalue_13',['FloatValue',['../classtinyxml2_1_1_x_m_l_attribute.html#a27797b45d21c981257720db94f5f8801',1,'tinyxml2::XMLAttribute']]],
  ['for_20optimization_14',['MeLOn - Machine Learning models for Optimization',['../index.html',1,'']]],
  ['forward_20neural_20network_15',['Feed forward neural network',['../ffnet.html',1,'models']]],
  ['free_16',['Free',['../classtinyxml2_1_1_mem_pool.html#a49e3bfac2cba2ebd6776b31e571f64f7',1,'tinyxml2::MemPool::Free()'],['../classtinyxml2_1_1_mem_pool_t.html#a408ce0918e9d3d5e5e1cc4896944875f',1,'tinyxml2::MemPoolT::Free()']]],
  ['from_5fjson_17',['from_json',['../gp_parser_8cpp.html#a0a8b2f8f1960f7555201a8275068177a',1,'from_json(const json &amp;j, GPData &amp;d):&#160;gpParser.cpp'],['../svm_parser_8cpp.html#ab8d477ea52b8fffb1bbdec0975f45a51',1,'from_json(const json &amp;j, SvmData &amp;d):&#160;svmParser.cpp']]]
];
