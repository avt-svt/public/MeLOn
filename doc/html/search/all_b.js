var searchData=
[
  ['k_0',['K',['../structmelon_1_1_g_p_data.html#af5a6789b5fc9709e3da7ff6a3ab8d417',1,'melon::GPData']]],
  ['kernel_1',['Kernel',['../classmelon_1_1kernel_1_1_kernel.html',1,'melon::kernel']]],
  ['kernel_2',['kernel',['../classmelon_1_1_gaussian_process.html#ab97b001f7292c45711747d1c8a2af0c4',1,'melon::GaussianProcess']]],
  ['kernel_2eh_3',['kernel.h',['../kernel_8h.html',1,'']]],
  ['kernel_5ffunction_4',['KERNEL_FUNCTION',['../namespacemelon.html#ae9d1641da9e08a0d25d8d6cefcc92887',1,'melon']]],
  ['kernelcompositeadd_5',['KernelCompositeAdd',['../classmelon_1_1kernel_1_1_kernel_composite_add.html',1,'melon::kernel']]],
  ['kernelcompositemultiply_6',['KernelCompositeMultiply',['../classmelon_1_1kernel_1_1_kernel_composite_multiply.html',1,'melon::kernel']]],
  ['kernelconstant_7',['KernelConstant',['../classmelon_1_1kernel_1_1_kernel_constant.html',1,'melon::kernel::KernelConstant&lt; T, V &gt;'],['../classmelon_1_1kernel_1_1_kernel_constant.html#a90c34c40cc180f0c3fe8b48f3e26ea77',1,'melon::kernel::KernelConstant::KernelConstant()'],['../classmelon_1_1kernel_1_1_kernel_constant.html#a8ae85b101e179697f2a8b71e3aa60f49',1,'melon::kernel::KernelConstant::KernelConstant(const T f)'],['../classmelon_1_1kernel_1_1_kernel_constant.html#ab46d81c3502ecef318700c88fbeb7dee',1,'melon::kernel::KernelConstant::KernelConstant(const V f)']]],
  ['kerneldata_8',['KernelData',['../structmelon_1_1kernel_1_1_kernel_data.html',1,'melon::kernel']]],
  ['kerneldata_9',['kernelData',['../structmelon_1_1_g_p_data.html#abf55d00a79969bcb9ddcb74575b6a5ab',1,'melon::GPData']]],
  ['kernelfunction_10',['kernelFunction',['../structmelon_1_1_svm_data.html#a63b5cf742541499e1f7cb0a3bae170bd',1,'melon::SvmData']]],
  ['kernelparameters_11',['kernelParameters',['../structmelon_1_1_svm_data.html#a4584851c196fdb3a1f53ea734cb24ca7',1,'melon::SvmData']]],
  ['kernelrbf_12',['KernelRBF',['../classmelon_1_1kernel_1_1_kernel_r_b_f.html',1,'melon::kernel::KernelRBF&lt; T, V &gt;'],['../classmelon_1_1kernel_1_1_kernel_r_b_f.html#aebf09200ca3535f515384f9326978359',1,'melon::kernel::KernelRBF::KernelRBF()']]]
];
