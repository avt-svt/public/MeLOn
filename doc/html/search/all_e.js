var searchData=
[
  ['name_0',['Name',['../classtinyxml2_1_1_x_m_l_attribute.html#a5a5c135d24cce7abda6f17301c6274d8',1,'tinyxml2::XMLAttribute::Name()'],['../classtinyxml2_1_1_x_m_l_element.html#abd36e34e4428a8eeeffbe87eab0b124d',1,'tinyxml2::XMLElement::Name()']]],
  ['needs_5fdelete_1',['NEEDS_DELETE',['../classtinyxml2_1_1_str_pair.html#a476a92d76f24486c3ae4731916b12aaeab9a3152ce5df9e7f4bbf3774fe862c75',1,'tinyxml2::StrPair']]],
  ['needs_5fentity_5fprocessing_2',['NEEDS_ENTITY_PROCESSING',['../classtinyxml2_1_1_str_pair.html#a0301ef962e15dd94574431f1c61266c5a4f1e01a55f8efe4ca72c32d454060237',1,'tinyxml2::StrPair']]],
  ['needs_5fflush_3',['NEEDS_FLUSH',['../classtinyxml2_1_1_str_pair.html#a476a92d76f24486c3ae4731916b12aaea2d8841daedc3955ed20ec9f760318434',1,'tinyxml2::StrPair']]],
  ['needs_5fnewline_5fnormalization_4',['NEEDS_NEWLINE_NORMALIZATION',['../classtinyxml2_1_1_str_pair.html#a0301ef962e15dd94574431f1c61266c5a8f2045d56e70745d718672c0da91d0e0',1,'tinyxml2::StrPair']]],
  ['needs_5fwhitespace_5fcollapsing_5',['NEEDS_WHITESPACE_COLLAPSING',['../classtinyxml2_1_1_str_pair.html#a0301ef962e15dd94574431f1c61266c5a13996e9d4ed18fd2d6af59bbab291b63',1,'tinyxml2::StrPair']]],
  ['network_6',['Feed forward neural network',['../ffnet.html',1,'models']]],
  ['neural_20network_7',['Feed forward neural network',['../ffnet.html',1,'models']]],
  ['newcomment_8',['NewComment',['../classtinyxml2_1_1_x_m_l_document.html#a386df0befd06aadb5e0cd21381aa955a',1,'tinyxml2::XMLDocument']]],
  ['newdeclaration_9',['NewDeclaration',['../classtinyxml2_1_1_x_m_l_document.html#ae519030c0262fa2daff8993681990e16',1,'tinyxml2::XMLDocument']]],
  ['newelement_10',['NewElement',['../classtinyxml2_1_1_x_m_l_document.html#a3c335a700a43d7c363a393142a23f234',1,'tinyxml2::XMLDocument']]],
  ['newtext_11',['NewText',['../classtinyxml2_1_1_x_m_l_document.html#acece5de77a0819f2341b08c1e1ed9987',1,'tinyxml2::XMLDocument']]],
  ['newunknown_12',['NewUnknown',['../classtinyxml2_1_1_x_m_l_document.html#a4954f502c5fd7f49de54c3c0c99bb73d',1,'tinyxml2::XMLDocument']]],
  ['next_13',['Next',['../classtinyxml2_1_1_x_m_l_attribute.html#aa8c7cf4c94a636ae75046658e551614e',1,'tinyxml2::XMLAttribute']]],
  ['next_14',['next',['../uniontinyxml2_1_1_mem_pool_t_1_1_item.html#a5620107f518c60d6619e8662d4c9d643',1,'tinyxml2::MemPoolT::Item']]],
  ['nextsibling_15',['NextSibling',['../classtinyxml2_1_1_x_m_l_node.html#af986c568061b020cf6232bec091388c0',1,'tinyxml2::XMLNode::NextSibling() const'],['../classtinyxml2_1_1_x_m_l_node.html#a05d04a9c06dc48ff8473c77774ee94eb',1,'tinyxml2::XMLNode::NextSibling()'],['../classtinyxml2_1_1_x_m_l_handle.html#aad2eccc7c7c7b18145877c978c3850b5',1,'tinyxml2::XMLHandle::NextSibling()'],['../classtinyxml2_1_1_x_m_l_const_handle.html#aec3710e455f41014026ef17fbbb0efb3',1,'tinyxml2::XMLConstHandle::NextSibling()']]],
  ['nextsiblingelement_16',['NextSiblingElement',['../classtinyxml2_1_1_x_m_l_node.html#a14ea560df31110ff07a9f566171bf797',1,'tinyxml2::XMLNode::NextSiblingElement(const char *name=0) const'],['../classtinyxml2_1_1_x_m_l_node.html#a83f054589f5a669c0dcca72bfd6bfb09',1,'tinyxml2::XMLNode::NextSiblingElement(const char *name=0)'],['../classtinyxml2_1_1_x_m_l_handle.html#ae41d88ee061f3c49a081630ff753b2c5',1,'tinyxml2::XMLHandle::NextSiblingElement()'],['../classtinyxml2_1_1_x_m_l_const_handle.html#a3c9e6b48b02d3d5232e1e8780753d8a5',1,'tinyxml2::XMLConstHandle::NextSiblingElement()']]],
  ['nochildren_17',['NoChildren',['../classtinyxml2_1_1_x_m_l_node.html#ac3ab489e6e202a3cd1762d3b332e89d4',1,'tinyxml2::XMLNode']]],
  ['normalizedoutput_18',['normalizedOutput',['../structmelon_1_1_ann_structure.html#a9506ef28c29e149fe0283ad7311fa275',1,'melon::AnnStructure']]],
  ['numlayers_19',['numLayers',['../structmelon_1_1_ann_structure.html#a6a3a954be4652ddccc8790fca4830a3b',1,'melon::AnnStructure']]],
  ['nx_20',['nX',['../structmelon_1_1_g_p_data.html#af94460988623f8f8f5debb885629ed58',1,'melon::GPData']]]
];
