var searchData=
[
  ['data_0',['data',['../classmelon_1_1_gaussian_process.html#a1e8c3d8b831d23f84ed287936cfddee6',1,'melon::GaussianProcess::data()'],['../classmelon_1_1_mulfil_gp.html#aca3166a1e6a5c3c2bd90f6ab9e80e87f',1,'melon::MulfilGp::data()']]],
  ['deepclone_1',['DeepClone',['../classtinyxml2_1_1_x_m_l_node.html#a3bb369fd733f1989b751d99a9417adab',1,'tinyxml2::XMLNode']]],
  ['deepcopy_2',['DeepCopy',['../classtinyxml2_1_1_x_m_l_document.html#af592ffc91514e25a39664521ac83db45',1,'tinyxml2::XMLDocument']]],
  ['deleteattribute_3',['DeleteAttribute',['../classtinyxml2_1_1_x_m_l_element.html#aebd45aa7118964c30b32fe12e944628a',1,'tinyxml2::XMLElement::DeleteAttribute(const char *name)'],['../classtinyxml2_1_1_x_m_l_element.html#afaab6a1f32caed4d37e6d7f130d277ac',1,'tinyxml2::XMLElement::DeleteAttribute(XMLAttribute *attribute)']]],
  ['deletechild_4',['DeleteChild',['../classtinyxml2_1_1_x_m_l_node.html#a363b6edbd6ebd55f8387d2b89f2b0921',1,'tinyxml2::XMLNode']]],
  ['deletechildren_5',['DeleteChildren',['../classtinyxml2_1_1_x_m_l_node.html#a0360085cc54df5bff85d5c5da13afdce',1,'tinyxml2::XMLNode']]],
  ['deletenode_6',['DeleteNode',['../classtinyxml2_1_1_x_m_l_node.html#ab0b21951f2343534c5c67f23dffdc7e6',1,'tinyxml2::XMLNode::DeleteNode()'],['../classtinyxml2_1_1_x_m_l_document.html#ac1d6e2c7fcc1a660624ac4f68e96380d',1,'tinyxml2::XMLDocument::DeleteNode()']]],
  ['depthtracker_7',['DepthTracker',['../classtinyxml2_1_1_x_m_l_document_1_1_depth_tracker.html#ac2782a163c2da773b84cb2c610b79bcb',1,'tinyxml2::XMLDocument::DepthTracker']]],
  ['descale_8',['descale',['../classmelon_1_1_scaler.html#acc1ba354a08076af07295b7b95ddf139',1,'melon::Scaler::descale()'],['../classmelon_1_1_identity_scaler.html#a59da1a53d947d4be17777524a3120d43',1,'melon::IdentityScaler::descale()'],['../classmelon_1_1_min_max_scaler.html#afe53d7d2569c3b06f52547a940846a98',1,'melon::MinMaxScaler::descale()'],['../classmelon_1_1_standard_scaler.html#a8afb1da58f6717f5b3900567fc82d841',1,'melon::StandardScaler::descale()']]],
  ['diag_9',['diag',['../namespacemelon.html#a16df369db8ed77a096d88bcac6c37487',1,'melon']]],
  ['dot_5fproduct_10',['dot_product',['../namespacemelon.html#a9cb84438576c7ac32cbe6f10b3e9e2b3',1,'melon']]],
  ['doubleattribute_11',['DoubleAttribute',['../classtinyxml2_1_1_x_m_l_element.html#a10a90c505aea716bf073eea1c97f33b5',1,'tinyxml2::XMLElement']]],
  ['doubletext_12',['DoubleText',['../classtinyxml2_1_1_x_m_l_element.html#a81b1ff0cf2f2cd09be8badc08b39a2b7',1,'tinyxml2::XMLElement']]],
  ['doublevalue_13',['DoubleValue',['../classtinyxml2_1_1_x_m_l_attribute.html#a4aa73513f54ff0087d3e804f0f54e30f',1,'tinyxml2::XMLAttribute']]],
  ['dynarray_14',['DynArray',['../classtinyxml2_1_1_dyn_array.html#aaad72f384e761c70a4519183eb8fea17',1,'tinyxml2::DynArray::DynArray()'],['../classtinyxml2_1_1_dyn_array.html#a8e2251588f079f2e7a4080b2c53dabea',1,'tinyxml2::DynArray::DynArray(const DynArray &amp;)']]]
];
