var searchData=
[
  ['kernel_0',['kernel',['../classmelon_1_1_gaussian_process.html#ab97b001f7292c45711747d1c8a2af0c4',1,'melon::GaussianProcess']]],
  ['kernelconstant_1',['KernelConstant',['../classmelon_1_1kernel_1_1_kernel_constant.html#a90c34c40cc180f0c3fe8b48f3e26ea77',1,'melon::kernel::KernelConstant::KernelConstant()'],['../classmelon_1_1kernel_1_1_kernel_constant.html#a8ae85b101e179697f2a8b71e3aa60f49',1,'melon::kernel::KernelConstant::KernelConstant(const T f)'],['../classmelon_1_1kernel_1_1_kernel_constant.html#ab46d81c3502ecef318700c88fbeb7dee',1,'melon::kernel::KernelConstant::KernelConstant(const V f)']]],
  ['kernelrbf_2',['KernelRBF',['../classmelon_1_1kernel_1_1_kernel_r_b_f.html#aebf09200ca3535f515384f9326978359',1,'melon::kernel::KernelRBF']]]
];
