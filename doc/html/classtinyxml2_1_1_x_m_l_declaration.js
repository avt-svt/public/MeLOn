var classtinyxml2_1_1_x_m_l_declaration =
[
    [ "XMLDeclaration", "classtinyxml2_1_1_x_m_l_declaration.html#aef9586f2ce5df5feba74dde49a242b06", null ],
    [ "~XMLDeclaration", "classtinyxml2_1_1_x_m_l_declaration.html#ab93d5bf4f5d58b4144963cf739cf6dcc", null ],
    [ "XMLDeclaration", "classtinyxml2_1_1_x_m_l_declaration.html#a5229cc0b31f034f93289af27ec3e2836", null ],
    [ "Accept", "classtinyxml2_1_1_x_m_l_declaration.html#acf47629d9fc08ed6f1c164a97bcf794b", null ],
    [ "operator=", "classtinyxml2_1_1_x_m_l_declaration.html#a19a67f4d9cb5aaf57c7767864e32f6b8", null ],
    [ "ParseDeep", "classtinyxml2_1_1_x_m_l_declaration.html#a42a2a36f4d78dc745063b79c16538b9b", null ],
    [ "ShallowClone", "classtinyxml2_1_1_x_m_l_declaration.html#ad9d60e6d2df75c13eb6bf7319985b747", null ],
    [ "ShallowEqual", "classtinyxml2_1_1_x_m_l_declaration.html#ae8b4d3a399857029f36c322b0801b69c", null ],
    [ "ToDeclaration", "classtinyxml2_1_1_x_m_l_declaration.html#aa24eb89ab36119e0b18ee06c82ff6c24", null ],
    [ "ToDeclaration", "classtinyxml2_1_1_x_m_l_declaration.html#a74e324fe0195ed20d7c2cc33adc0728c", null ],
    [ "XMLDocument", "classtinyxml2_1_1_x_m_l_declaration.html#a4eee3bda60c60a30e4e8cd4ea91c4c6e", null ]
];