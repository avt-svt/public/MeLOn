var classmelon_1_1_standard_scaler =
[
    [ "StandardScaler", "classmelon_1_1_standard_scaler.html#aa24965025a22213f80a98392b21f2f7c", null ],
    [ "descale", "classmelon_1_1_standard_scaler.html#a8afb1da58f6717f5b3900567fc82d841", null ],
    [ "scale", "classmelon_1_1_standard_scaler.html#adbd404ec388534902787f3018e2becfd", null ],
    [ "_mean", "classmelon_1_1_standard_scaler.html#a0f535a7405baecfb7a3c3ab41494872d", null ],
    [ "_stdDev", "classmelon_1_1_standard_scaler.html#ac34dd7674c62dcb2c33925ffeafd810b", null ]
];