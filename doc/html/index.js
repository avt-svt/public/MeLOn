var index =
[
    [ "Introduction", "index.html#intro_sec", null ],
    [ "Installing and Executing MeLOn", "install.html", [
      [ "Obtaining MeLOn", "install.html#get_melon", [
        [ "Updating MeLOn", "install.html#update_melon", null ]
      ] ],
      [ "Required Software", "install.html#req_software", null ],
      [ "Generating and Compiling the Project", "install.html#cmake", [
        [ "Windows", "install.html#cmake_win", null ],
        [ "Linux and Mac OS", "install.html#cmake_linux_os", null ]
      ] ]
    ] ],
    [ "Models", "models.html", "models" ],
    [ "Where can I read more?", "bib.html", [
      [ "Uses of MAiNGO", "bib.html#readUses", null ]
    ] ]
];