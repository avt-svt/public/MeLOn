var scaler_8h =
[
    [ "melon::ScalerData", "structmelon_1_1_scaler_data.html", "structmelon_1_1_scaler_data" ],
    [ "melon::Scaler< T >", "classmelon_1_1_scaler.html", "classmelon_1_1_scaler" ],
    [ "melon::ScalerFactory< T >", "classmelon_1_1_scaler_factory.html", null ],
    [ "melon::IdentityScaler< T >", "classmelon_1_1_identity_scaler.html", "classmelon_1_1_identity_scaler" ],
    [ "melon::MinMaxScaler< T >", "classmelon_1_1_min_max_scaler.html", "classmelon_1_1_min_max_scaler" ],
    [ "melon::StandardScaler< T >", "classmelon_1_1_standard_scaler.html", "classmelon_1_1_standard_scaler" ],
    [ "SCALER_PARAMETER", "scaler_8h.html#a7c96133788161408370e83766fc229ce", [
      [ "LOWER_BOUNDS", "scaler_8h.html#a7c96133788161408370e83766fc229ceaf78372f3432f97adeaeb2af268197309", null ],
      [ "UPPER_BOUNDS", "scaler_8h.html#a7c96133788161408370e83766fc229ceac08c5be55fca3df3d17b9195c771513b", null ],
      [ "STD_DEV", "scaler_8h.html#a7c96133788161408370e83766fc229ceab0822e223d8cf003bdb0b83f0134e6e4", null ],
      [ "MEAN", "scaler_8h.html#a7c96133788161408370e83766fc229cea2ad43cbc6545d4304492db86d53638db", null ],
      [ "SCALED_LOWER_BOUNDS", "scaler_8h.html#a7c96133788161408370e83766fc229cea70d9219e0e27a9c129ebfdb80b6b907f", null ],
      [ "SCALED_UPPER_BOUNDS", "scaler_8h.html#a7c96133788161408370e83766fc229cea92a33be48e968102397691173990fe32", null ]
    ] ],
    [ "SCALER_TYPE", "scaler_8h.html#a7ee65995944832d7b450202201d45c11", [
      [ "IDENTITY", "scaler_8h.html#a7ee65995944832d7b450202201d45c11afaeaa7edb255915742a7160e41431ac3", null ],
      [ "MINMAX", "scaler_8h.html#a7ee65995944832d7b450202201d45c11ac7872fa543809084c52b6271d3fdfebf", null ],
      [ "STANDARD", "scaler_8h.html#a7ee65995944832d7b450202201d45c11acdd23303789346092329efb21bb94a62", null ]
    ] ]
];