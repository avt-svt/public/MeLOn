var classmelon_1_1_feed_forward_net =
[
    [ "FeedForwardNet", "classmelon_1_1_feed_forward_net.html#a40d5999e0803420f02e2285aee4d7f19", null ],
    [ "FeedForwardNet", "classmelon_1_1_feed_forward_net.html#ac1ac26b7d389470fc0a13463cddd9a3f", null ],
    [ "FeedForwardNet", "classmelon_1_1_feed_forward_net.html#aa5bfb8592ff40db349c0699f22dcaf6e", null ],
    [ "FeedForwardNet", "classmelon_1_1_feed_forward_net.html#a5aa1638cb722f3139dd95f229d0ba0a2", null ],
    [ "~FeedForwardNet", "classmelon_1_1_feed_forward_net.html#a30df96721f1c57e57ed668676f18d9dc", null ],
    [ "_calculate_layer_activation", "classmelon_1_1_feed_forward_net.html#a52bc8b4239937c6290e3a2b59dad9f24", null ],
    [ "_calculate_prediction", "classmelon_1_1_feed_forward_net.html#a863db6c1f3e0937db86ecbb59cacf635", null ],
    [ "_set_data_object", "classmelon_1_1_feed_forward_net.html#ae98f7e952148f9d8aa22d2450f50f9db", null ],
    [ "calculate_prediction_full_space", "classmelon_1_1_feed_forward_net.html#a1bbcf42c9cb486f063d8a47722c7a209", null ],
    [ "calculate_prediction_reduced_space", "classmelon_1_1_feed_forward_net.html#aee07aaae68255e370ba1e82c01229cdd", null ],
    [ "get_full_space_variables", "classmelon_1_1_feed_forward_net.html#a9a6336300c3e798cb58832019e701bc4", null ],
    [ "get_number_of_full_space_variables", "classmelon_1_1_feed_forward_net.html#a47ce1749d5969289495ad8a84e438c70", null ],
    [ "set_tanh_formulation", "classmelon_1_1_feed_forward_net.html#a577ee52dca91fe4d987484afa8bf33a8", null ],
    [ "_annData", "classmelon_1_1_feed_forward_net.html#a3b6e46ca8afad6af62b3f15e2b6b5ec6", null ],
    [ "_inputScaler", "classmelon_1_1_feed_forward_net.html#a8d22eb1e65b7f74b4e84c3c8ea78df51", null ],
    [ "_outputScaler", "classmelon_1_1_feed_forward_net.html#a25f7c656e140792b73fccc7058249256", null ],
    [ "_tanh_formulation", "classmelon_1_1_feed_forward_net.html#aa29c58f20ab3b344243565940fc0e243", null ]
];