var hierarchy =
[
    [ "melon::AnnNormalizationParameters", "structmelon_1_1_ann_normalization_parameters.html", null ],
    [ "melon::AnnStructure", "structmelon_1_1_ann_structure.html", null ],
    [ "melon::AnnWeights", "structmelon_1_1_ann_weights.html", null ],
    [ "tinyxml2::MemPoolT< ITEM_SIZE >::Block", "structtinyxml2_1_1_mem_pool_t_1_1_block.html", null ],
    [ "tinyxml2::XMLDocument::DepthTracker", "classtinyxml2_1_1_x_m_l_document_1_1_depth_tracker.html", null ],
    [ "tinyxml2::DynArray< T, INITIAL_SIZE >", "classtinyxml2_1_1_dyn_array.html", null ],
    [ "tinyxml2::DynArray< char, 20 >", "classtinyxml2_1_1_dyn_array.html", null ],
    [ "tinyxml2::DynArray< const char *, 10 >", "classtinyxml2_1_1_dyn_array.html", null ],
    [ "tinyxml2::DynArray< tinyxml2::MemPoolT::Block *, 10 >", "classtinyxml2_1_1_dyn_array.html", null ],
    [ "tinyxml2::DynArray< tinyxml2::XMLNode *, 10 >", "classtinyxml2_1_1_dyn_array.html", null ],
    [ "tinyxml2::Entity", "structtinyxml2_1_1_entity.html", null ],
    [ "std::exception", null, [
      [ "MelonException", "class_melon_exception.html", null ]
    ] ],
    [ "tinyxml2::MemPoolT< ITEM_SIZE >::Item", "uniontinyxml2_1_1_mem_pool_t_1_1_item.html", null ],
    [ "melon::kernel::Kernel< T, V >", "classmelon_1_1kernel_1_1_kernel.html", [
      [ "melon::kernel::KernelCompositeAdd< T, V >", "classmelon_1_1kernel_1_1_kernel_composite_add.html", null ],
      [ "melon::kernel::KernelCompositeMultiply< T, V >", "classmelon_1_1kernel_1_1_kernel_composite_multiply.html", null ],
      [ "melon::kernel::KernelConstant< T, V >", "classmelon_1_1kernel_1_1_kernel_constant.html", null ],
      [ "melon::kernel::StationaryKernel< T, V >", "classmelon_1_1kernel_1_1_stationary_kernel.html", [
        [ "melon::kernel::KernelRBF< T, V >", "classmelon_1_1kernel_1_1_kernel_r_b_f.html", null ],
        [ "melon::kernel::Matern< T, V >", "classmelon_1_1kernel_1_1_matern.html", [
          [ "melon::kernel::Matern12< T, V >", "classmelon_1_1kernel_1_1_matern12.html", null ],
          [ "melon::kernel::Matern32< T, V >", "classmelon_1_1kernel_1_1_matern32.html", null ],
          [ "melon::kernel::Matern52< T, V >", "classmelon_1_1kernel_1_1_matern52.html", null ],
          [ "melon::kernel::MaternInf< T, V >", "classmelon_1_1kernel_1_1_matern_inf.html", null ]
        ] ]
      ] ]
    ] ],
    [ "melon::kernel::KernelData", "structmelon_1_1kernel_1_1_kernel_data.html", null ],
    [ "tinyxml2::LongFitsIntoSizeTMinusOne< bool >", "structtinyxml2_1_1_long_fits_into_size_t_minus_one.html", null ],
    [ "tinyxml2::LongFitsIntoSizeTMinusOne< false >", "structtinyxml2_1_1_long_fits_into_size_t_minus_one_3_01false_01_4.html", null ],
    [ "melon::MelonModel< T >", "classmelon_1_1_melon_model.html", [
      [ "melon::FeedForwardNet< T >", "classmelon_1_1_feed_forward_net.html", null ],
      [ "melon::GaussianProcess< T >", "classmelon_1_1_gaussian_process.html", null ],
      [ "melon::MulfilGp< T >", "classmelon_1_1_mulfil_gp.html", null ],
      [ "melon::SupportVectorMachine< T >", "classmelon_1_1_support_vector_machine.html", [
        [ "melon::SupportVectorMachineOneClass< T >", "classmelon_1_1_support_vector_machine_one_class.html", null ],
        [ "melon::SupportVectorRegression< T >", "classmelon_1_1_support_vector_regression.html", null ]
      ] ]
    ] ],
    [ "tinyxml2::MemPool", "classtinyxml2_1_1_mem_pool.html", [
      [ "tinyxml2::MemPoolT< sizeof(tinyxml2::XMLElement) >", "classtinyxml2_1_1_mem_pool_t.html", null ],
      [ "tinyxml2::MemPoolT< sizeof(tinyxml2::XMLAttribute) >", "classtinyxml2_1_1_mem_pool_t.html", null ],
      [ "tinyxml2::MemPoolT< sizeof(tinyxml2::XMLText) >", "classtinyxml2_1_1_mem_pool_t.html", null ],
      [ "tinyxml2::MemPoolT< sizeof(tinyxml2::XMLComment) >", "classtinyxml2_1_1_mem_pool_t.html", null ],
      [ "tinyxml2::MemPoolT< ITEM_SIZE >", "classtinyxml2_1_1_mem_pool_t.html", null ]
    ] ],
    [ "ModelData", "struct_model_data.html", [
      [ "melon::AnnData", "structmelon_1_1_ann_data.html", null ],
      [ "melon::GPData", "structmelon_1_1_g_p_data.html", null ],
      [ "melon::MulfilGpData", "structmelon_1_1_mulfil_gp_data.html", null ],
      [ "melon::SvmData", "structmelon_1_1_svm_data.html", null ]
    ] ],
    [ "melon::ModelParser", "classmelon_1_1_model_parser.html", [
      [ "melon::AnnParser", "classmelon_1_1_ann_parser.html", [
        [ "melon::AnnParserCsv", "classmelon_1_1_ann_parser_csv.html", null ],
        [ "melon::AnnParserXml", "classmelon_1_1_ann_parser_xml.html", null ]
      ] ],
      [ "melon::GpParser", "classmelon_1_1_gp_parser.html", null ],
      [ "melon::MulfilGpParser", "classmelon_1_1_mulfil_gp_parser.html", null ],
      [ "melon::SvmParser", "classmelon_1_1_svm_parser.html", null ]
    ] ],
    [ "melon::ModelParserFactory", "classmelon_1_1_model_parser_factory.html", [
      [ "melon::AnnParserFactory", "classmelon_1_1_ann_parser_factory.html", null ],
      [ "melon::GpParserFactory", "classmelon_1_1_gp_parser_factory.html", null ],
      [ "melon::MulfilGpParserFactory", "classmelon_1_1_mulfil_gp_parser_factory.html", null ],
      [ "melon::SvmParserFactory", "classmelon_1_1_svm_parser_factory.html", null ]
    ] ],
    [ "MulFilGpData", "struct_mul_fil_gp_data.html", null ],
    [ "melon::Scaler< T >", "classmelon_1_1_scaler.html", [
      [ "melon::IdentityScaler< T >", "classmelon_1_1_identity_scaler.html", null ],
      [ "melon::MinMaxScaler< T >", "classmelon_1_1_min_max_scaler.html", null ],
      [ "melon::StandardScaler< T >", "classmelon_1_1_standard_scaler.html", null ]
    ] ],
    [ "melon::ScalerData", "structmelon_1_1_scaler_data.html", null ],
    [ "melon::ScalerFactory< T >", "classmelon_1_1_scaler_factory.html", null ],
    [ "Stationary", "class_stationary.html", null ],
    [ "tinyxml2::StrPair", "classtinyxml2_1_1_str_pair.html", null ],
    [ "tinyxml2::XMLAttribute", "classtinyxml2_1_1_x_m_l_attribute.html", null ],
    [ "tinyxml2::XMLConstHandle", "classtinyxml2_1_1_x_m_l_const_handle.html", null ],
    [ "tinyxml2::XMLHandle", "classtinyxml2_1_1_x_m_l_handle.html", null ],
    [ "tinyxml2::XMLNode", "classtinyxml2_1_1_x_m_l_node.html", [
      [ "tinyxml2::XMLComment", "classtinyxml2_1_1_x_m_l_comment.html", null ],
      [ "tinyxml2::XMLDeclaration", "classtinyxml2_1_1_x_m_l_declaration.html", null ],
      [ "tinyxml2::XMLDocument", "classtinyxml2_1_1_x_m_l_document.html", null ],
      [ "tinyxml2::XMLElement", "classtinyxml2_1_1_x_m_l_element.html", null ],
      [ "tinyxml2::XMLText", "classtinyxml2_1_1_x_m_l_text.html", null ],
      [ "tinyxml2::XMLUnknown", "classtinyxml2_1_1_x_m_l_unknown.html", null ]
    ] ],
    [ "tinyxml2::XMLUtil", "classtinyxml2_1_1_x_m_l_util.html", null ],
    [ "tinyxml2::XMLVisitor", "classtinyxml2_1_1_x_m_l_visitor.html", [
      [ "tinyxml2::XMLPrinter", "classtinyxml2_1_1_x_m_l_printer.html", null ]
    ] ]
];