var classmelon_1_1kernel_1_1_kernel_r_b_f =
[
    [ "KernelRBF", "classmelon_1_1kernel_1_1_kernel_r_b_f.html#aebf09200ca3535f515384f9326978359", null ],
    [ "calculate_distance", "classmelon_1_1kernel_1_1_kernel_r_b_f.html#a1fb033bbe5a2ed283fcecc2a0a34bb84", null ],
    [ "evaluate_kernel", "classmelon_1_1kernel_1_1_kernel_r_b_f.html#a8bfc02f818e464515cf4eee385e4451e", null ],
    [ "evaluate_kernel", "classmelon_1_1kernel_1_1_kernel_r_b_f.html#af77cb8a39caa7d5191aa560eb80d2f09", null ],
    [ "_gamma", "classmelon_1_1kernel_1_1_kernel_r_b_f.html#a8885cc1731e50ae7b5f9bc0bef44748e", null ]
];