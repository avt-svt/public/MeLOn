var namespacemelon =
[
    [ "kernel", "namespacemelon_1_1kernel.html", "namespacemelon_1_1kernel" ],
    [ "AnnData", "structmelon_1_1_ann_data.html", "structmelon_1_1_ann_data" ],
    [ "AnnNormalizationParameters", "structmelon_1_1_ann_normalization_parameters.html", "structmelon_1_1_ann_normalization_parameters" ],
    [ "AnnParser", "classmelon_1_1_ann_parser.html", "classmelon_1_1_ann_parser" ],
    [ "AnnParserCsv", "classmelon_1_1_ann_parser_csv.html", "classmelon_1_1_ann_parser_csv" ],
    [ "AnnParserFactory", "classmelon_1_1_ann_parser_factory.html", "classmelon_1_1_ann_parser_factory" ],
    [ "AnnParserXml", "classmelon_1_1_ann_parser_xml.html", "classmelon_1_1_ann_parser_xml" ],
    [ "AnnStructure", "structmelon_1_1_ann_structure.html", "structmelon_1_1_ann_structure" ],
    [ "AnnWeights", "structmelon_1_1_ann_weights.html", "structmelon_1_1_ann_weights" ],
    [ "FeedForwardNet", "classmelon_1_1_feed_forward_net.html", "classmelon_1_1_feed_forward_net" ],
    [ "GaussianProcess", "classmelon_1_1_gaussian_process.html", "classmelon_1_1_gaussian_process" ],
    [ "GPData", "structmelon_1_1_g_p_data.html", "structmelon_1_1_g_p_data" ],
    [ "GpParser", "classmelon_1_1_gp_parser.html", "classmelon_1_1_gp_parser" ],
    [ "GpParserFactory", "classmelon_1_1_gp_parser_factory.html", "classmelon_1_1_gp_parser_factory" ],
    [ "IdentityScaler", "classmelon_1_1_identity_scaler.html", "classmelon_1_1_identity_scaler" ],
    [ "MelonModel", "classmelon_1_1_melon_model.html", "classmelon_1_1_melon_model" ],
    [ "MinMaxScaler", "classmelon_1_1_min_max_scaler.html", "classmelon_1_1_min_max_scaler" ],
    [ "ModelParser", "classmelon_1_1_model_parser.html", "classmelon_1_1_model_parser" ],
    [ "ModelParserFactory", "classmelon_1_1_model_parser_factory.html", "classmelon_1_1_model_parser_factory" ],
    [ "MulfilGp", "classmelon_1_1_mulfil_gp.html", "classmelon_1_1_mulfil_gp" ],
    [ "MulfilGpData", "structmelon_1_1_mulfil_gp_data.html", "structmelon_1_1_mulfil_gp_data" ],
    [ "MulfilGpParser", "classmelon_1_1_mulfil_gp_parser.html", "classmelon_1_1_mulfil_gp_parser" ],
    [ "MulfilGpParserFactory", "classmelon_1_1_mulfil_gp_parser_factory.html", "classmelon_1_1_mulfil_gp_parser_factory" ],
    [ "Scaler", "classmelon_1_1_scaler.html", "classmelon_1_1_scaler" ],
    [ "ScalerData", "structmelon_1_1_scaler_data.html", "structmelon_1_1_scaler_data" ],
    [ "ScalerFactory", "classmelon_1_1_scaler_factory.html", null ],
    [ "StandardScaler", "classmelon_1_1_standard_scaler.html", "classmelon_1_1_standard_scaler" ],
    [ "SupportVectorMachine", "classmelon_1_1_support_vector_machine.html", "classmelon_1_1_support_vector_machine" ],
    [ "SupportVectorMachineOneClass", "classmelon_1_1_support_vector_machine_one_class.html", "classmelon_1_1_support_vector_machine_one_class" ],
    [ "SupportVectorRegression", "classmelon_1_1_support_vector_regression.html", "classmelon_1_1_support_vector_regression" ],
    [ "SvmData", "structmelon_1_1_svm_data.html", "structmelon_1_1_svm_data" ],
    [ "SvmParser", "classmelon_1_1_svm_parser.html", "classmelon_1_1_svm_parser" ],
    [ "SvmParserFactory", "classmelon_1_1_svm_parser_factory.html", "classmelon_1_1_svm_parser_factory" ],
    [ "ACTIVATION_FUNCTION", "namespacemelon.html#a48f12885945641a41597e6668940dc73", [
      [ "PURE_LIN", "namespacemelon.html#a48f12885945641a41597e6668940dc73a2f195b9787f598a7ee661a881c474e3b", null ],
      [ "TANH", "namespacemelon.html#a48f12885945641a41597e6668940dc73ad3d7d3b5435748e6d590a93d042de6c2", null ],
      [ "RELU", "namespacemelon.html#a48f12885945641a41597e6668940dc73acb18be48f9330d10bfd46e4046f09d3c", null ],
      [ "RELU6", "namespacemelon.html#a48f12885945641a41597e6668940dc73a48a94bc178c62695a3d5142cb8b984ed", null ]
    ] ],
    [ "KERNEL_FUNCTION", "namespacemelon.html#ae9d1641da9e08a0d25d8d6cefcc92887", [
      [ "RBF", "namespacemelon.html#ae9d1641da9e08a0d25d8d6cefcc92887ae0c0181a2948647ea58c0940cce1b47e", null ]
    ] ],
    [ "MODEL_FILE_TYPE", "namespacemelon.html#a4304be33fc99f22bd328db5774394531", [
      [ "CSV", "namespacemelon.html#a4304be33fc99f22bd328db5774394531a6a25a8815b9ce41cb44aec7974135a23", null ],
      [ "XML", "namespacemelon.html#a4304be33fc99f22bd328db5774394531a85425286917a906087af8aa4f28b99e7", null ],
      [ "JSON", "namespacemelon.html#a4304be33fc99f22bd328db5774394531aaa1153b20d42a497e239ee88cc90fe6d", null ]
    ] ],
    [ "SCALER_PARAMETER", "namespacemelon.html#a7c96133788161408370e83766fc229ce", [
      [ "LOWER_BOUNDS", "namespacemelon.html#a7c96133788161408370e83766fc229ceaf78372f3432f97adeaeb2af268197309", null ],
      [ "UPPER_BOUNDS", "namespacemelon.html#a7c96133788161408370e83766fc229ceac08c5be55fca3df3d17b9195c771513b", null ],
      [ "STD_DEV", "namespacemelon.html#a7c96133788161408370e83766fc229ceab0822e223d8cf003bdb0b83f0134e6e4", null ],
      [ "MEAN", "namespacemelon.html#a7c96133788161408370e83766fc229cea2ad43cbc6545d4304492db86d53638db", null ],
      [ "SCALED_LOWER_BOUNDS", "namespacemelon.html#a7c96133788161408370e83766fc229cea70d9219e0e27a9c129ebfdb80b6b907f", null ],
      [ "SCALED_UPPER_BOUNDS", "namespacemelon.html#a7c96133788161408370e83766fc229cea92a33be48e968102397691173990fe32", null ]
    ] ],
    [ "SCALER_TYPE", "namespacemelon.html#a7ee65995944832d7b450202201d45c11", [
      [ "IDENTITY", "namespacemelon.html#a7ee65995944832d7b450202201d45c11afaeaa7edb255915742a7160e41431ac3", null ],
      [ "MINMAX", "namespacemelon.html#a7ee65995944832d7b450202201d45c11ac7872fa543809084c52b6271d3fdfebf", null ],
      [ "STANDARD", "namespacemelon.html#a7ee65995944832d7b450202201d45c11acdd23303789346092329efb21bb94a62", null ]
    ] ],
    [ "TANH_REFORMULATION", "namespacemelon.html#a8022b4f904d4418a296e665306c3ff45", [
      [ "TANH_REF_0", "namespacemelon.html#a8022b4f904d4418a296e665306c3ff45a0421f11571e68cf83bd0e9098c4a9b06", null ],
      [ "TANH_REF1", "namespacemelon.html#a8022b4f904d4418a296e665306c3ff45a4bc63b72e39fc9afbb0180ce9696ae44", null ],
      [ "TANH_REF2", "namespacemelon.html#a8022b4f904d4418a296e665306c3ff45aa92da5b44334a4d1b46e21b3e0d04e42", null ],
      [ "TANH_REF3", "namespacemelon.html#a8022b4f904d4418a296e665306c3ff45aa409ddee207d203ecccf9ed6fc1c008e", null ],
      [ "TANH_REF4", "namespacemelon.html#a8022b4f904d4418a296e665306c3ff45a34a691a87b4d07bbdda82aa924ce94b0", null ]
    ] ],
    [ "diag", "namespacemelon.html#a16df369db8ed77a096d88bcac6c37487", null ],
    [ "dot_product", "namespacemelon.html#a9cb84438576c7ac32cbe6f10b3e9e2b3", null ],
    [ "operator*", "namespacemelon.html#adfc1c64d377bc9a21eea10b08a2812bd", null ],
    [ "operator*", "namespacemelon.html#a40328bd6fab65711cf52ef704a727e63", null ],
    [ "operator*", "namespacemelon.html#aae6d2a726febc6962b2b4b60fe1bb472", null ],
    [ "operator*", "namespacemelon.html#a01c6cf5e8fc7f903c966ed77d3582789", null ],
    [ "operator+", "namespacemelon.html#ab2653a81de37c679fc48d8303b202614", null ],
    [ "operator+", "namespacemelon.html#a80e855bf708cd67182f551d005dea25e", null ],
    [ "operator-", "namespacemelon.html#a7bdd489b771e3b35186e3a89fc4520ad", null ],
    [ "operator-", "namespacemelon.html#a2b23af0670582490d233e2cfa02bbf7a", null ],
    [ "operator/", "namespacemelon.html#a24bae557d935851e04ec91570cdf6b46", null ],
    [ "transpose", "namespacemelon.html#aa014086872bff2237b6ffa78096b593f", null ]
];