var structmelon_1_1_ann_normalization_parameters =
[
    [ "inputLowerBound", "structmelon_1_1_ann_normalization_parameters.html#a9cbed7c9cf95196ed059a5fdd41d2a10", null ],
    [ "inputUpperBound", "structmelon_1_1_ann_normalization_parameters.html#ab68e5a77ffed12abe0d592651fcaf098", null ],
    [ "outputDenormalizationFactor", "structmelon_1_1_ann_normalization_parameters.html#a858157c7ba40e5d6c1e325ad5d719a3a", null ],
    [ "outputDenormalizationOffset", "structmelon_1_1_ann_normalization_parameters.html#a2e30539514daa68b2b23dad410a32010", null ]
];