var classmelon_1_1_support_vector_machine =
[
    [ "SupportVectorMachine", "classmelon_1_1_support_vector_machine.html#a1cf7bc508b3f3eb3e6aa2b43578ab867", null ],
    [ "SupportVectorMachine", "classmelon_1_1_support_vector_machine.html#a26c00f7ec0bfe43f85dc386ae4832916", null ],
    [ "SupportVectorMachine", "classmelon_1_1_support_vector_machine.html#a401274575c100133a03e8cbf118153d6", null ],
    [ "SupportVectorMachine", "classmelon_1_1_support_vector_machine.html#afbf424b9a1b23030fd3bb259d0b67800", null ],
    [ "_calculate_prediction", "classmelon_1_1_support_vector_machine.html#ac6565c54b8f61f6ad7e03dab3f56d5b0", null ],
    [ "_decision_function", "classmelon_1_1_support_vector_machine.html#abf0be2b3dd923d3594af7d93eecc9ca8", null ],
    [ "_set_data_object", "classmelon_1_1_support_vector_machine.html#ad1a0be4db1d52bf32f1651142a422a22", null ],
    [ "_update_kernel", "classmelon_1_1_support_vector_machine.html#a4b02218e266feb6cced0584c6f965a96", null ],
    [ "calculate_prediction_full_space", "classmelon_1_1_support_vector_machine.html#aa24acc1421812ac374878aa9f73007c8", null ],
    [ "calculate_prediction_reduced_space", "classmelon_1_1_support_vector_machine.html#af395a6dfdc70892d7ae11ddfda45936d", null ],
    [ "get_fullspace_variables", "classmelon_1_1_support_vector_machine.html#a2deaf0cc2520318c8208f3d552d040cf", null ],
    [ "get_number_of_full_space_variables", "classmelon_1_1_support_vector_machine.html#a0b66a6ddb023bd946815e7d1b885b273", null ],
    [ "_data", "classmelon_1_1_support_vector_machine.html#aa2b45da4783283842e9cd42846753315", null ],
    [ "_inputScaler", "classmelon_1_1_support_vector_machine.html#ae16e1a18a7c8a6237bba83c43f31ed29", null ],
    [ "_kernel", "classmelon_1_1_support_vector_machine.html#aeeac5223941266357a7711260527808a", null ],
    [ "_outputScaler", "classmelon_1_1_support_vector_machine.html#a6f2560dacef790d4668cbc27bea19178", null ]
];