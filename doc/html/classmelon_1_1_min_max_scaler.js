var classmelon_1_1_min_max_scaler =
[
    [ "MinMaxScaler", "classmelon_1_1_min_max_scaler.html#a10f2bd5aaa731136d7089afdf4cad3ed", null ],
    [ "descale", "classmelon_1_1_min_max_scaler.html#afe53d7d2569c3b06f52547a940846a98", null ],
    [ "scale", "classmelon_1_1_min_max_scaler.html#a82a108230095d1cbdfbb90e221d51cf4", null ],
    [ "_lowerBounds", "classmelon_1_1_min_max_scaler.html#a4692dd7c3dab76d4e189060a4bd494ca", null ],
    [ "_scaledLowerBounds", "classmelon_1_1_min_max_scaler.html#a40116fba518d7fff221d0ecc364c5631", null ],
    [ "_scaledUpperBounds", "classmelon_1_1_min_max_scaler.html#aa1908e8c3d60cf316eedb75869bf6bcb", null ],
    [ "_upperBounds", "classmelon_1_1_min_max_scaler.html#ac2d9d43ded890a37679f876fb9213a35", null ]
];