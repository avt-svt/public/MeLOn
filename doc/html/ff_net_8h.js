var ff_net_8h =
[
    [ "melon::FeedForwardNet< T >", "classmelon_1_1_feed_forward_net.html", "classmelon_1_1_feed_forward_net" ],
    [ "TANH_REFORMULATION", "ff_net_8h.html#a8022b4f904d4418a296e665306c3ff45", [
      [ "TANH_REF_0", "ff_net_8h.html#a8022b4f904d4418a296e665306c3ff45a0421f11571e68cf83bd0e9098c4a9b06", null ],
      [ "TANH_REF1", "ff_net_8h.html#a8022b4f904d4418a296e665306c3ff45a4bc63b72e39fc9afbb0180ce9696ae44", null ],
      [ "TANH_REF2", "ff_net_8h.html#a8022b4f904d4418a296e665306c3ff45aa92da5b44334a4d1b46e21b3e0d04e42", null ],
      [ "TANH_REF3", "ff_net_8h.html#a8022b4f904d4418a296e665306c3ff45aa409ddee207d203ecccf9ed6fc1c008e", null ],
      [ "TANH_REF4", "ff_net_8h.html#a8022b4f904d4418a296e665306c3ff45a34a691a87b4d07bbdda82aa924ce94b0", null ]
    ] ]
];