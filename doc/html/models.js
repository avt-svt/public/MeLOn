var models =
[
    [ "Feed forward neural network", "ffnet.html", [
      [ "Training", "ffnet.html#ffnet-training", [
        [ "Python", "ffnet.html#ffnet-python", [
          [ "Requirements", "ffnet.html#ffnet-python-requirements", null ],
          [ "Using the example scripts", "ffnet.html#ffnet-python-examples", null ],
          [ "Writing your own script", "ffnet.html#ffnet-python-own-script", null ]
        ] ],
        [ "Matlab", "ffnet.html#ffnet-matlab", null ]
      ] ],
      [ "Usage", "ffnet.html#ffnet-usage", [
        [ "Constructors", "ffnet.html#ffnet-usage-constructors", null ],
        [ "Prediction", "ffnet.html#ffnet-usage-prediction", null ],
        [ "Internal variables", "ffnet.html#ffnet-usage-internal-variables", null ],
        [ "Settings", "ffnet.html#ffnet-usage-settings", null ]
      ] ]
    ] ],
    [ "Gaussian process", "gp.html", [
      [ "Training", "gp.html#gp-training", [
        [ "Python", "gp.html#gp-python", [
          [ "Requirements", "gp.html#gp-python-training-requirements", null ],
          [ "Using the example scripts", "gp.html#gp-training-python-examples", null ]
        ] ],
        [ "Matlab", "gp.html#gp-matlab", null ]
      ] ],
      [ "Usage", "gp.html#gp-usage", [
        [ "Constructors", "gp.html#gp-usage-constructors", null ],
        [ "Prediction and variance", "gp.html#gp-usage-prediction", null ],
        [ "Internal variables", "gp.html#gp-usage-internal-variables", null ],
        [ "Information on training data", "gp.html#gp-usage-training-data", null ]
      ] ]
    ] ],
    [ "Multifidelity Gaussian process", "mulfil-gp.html", [
      [ "General information", "mulfil-gp.html#mulfil-gp-general", null ],
      [ "Training", "mulfil-gp.html#mulfil-gp-training", null ],
      [ "Example scripts", "mulfil-gp.html#mulfil-gp-examples", null ]
    ] ],
    [ "Support vector machines", "svm.html", [
      [ "Training", "svm.html#svm-training", [
        [ "Requirements", "svm.html#svm-training-requirements", null ],
        [ "Model", "svm.html#svm-training-model", [
          [ "Restrictions", "svm.html#svm-training-model-restrictions", null ]
        ] ],
        [ "Feature scaling", "svm.html#svm-training-scaling", [
          [ "Restrictions", "svm.html#svm-training-scaling-restrictions", null ]
        ] ],
        [ "Generating the model file.", "svm.html#svm-training-file", null ]
      ] ],
      [ "Usage", "svm.html#svm-usage", [
        [ "Constructors", "svm.html#svm-usage-constructors", [
          [ "Support vector machine (Abstract parent class: Can't be called!)", "svm.html#svm-usage-constructors-svm", null ],
          [ "Support vector regression", "svm.html#svm-usage-constructors-svr", null ],
          [ "One class support vector machine", "svm.html#svm-usage-constructors-ocsvm", null ]
        ] ],
        [ "Prediction", "svm.html#svm-usage-prediction", [
          [ "Support vector regression", "svm.html#svm-usage-prediction-svr", null ],
          [ "One class support vector machine", "svm.html#svm-usage-prediction-ocsvm", null ]
        ] ],
        [ "Internal variables", "svm.html#svm-usage-internal-variables", [
          [ "Support vector regression", "svm.html#svm-usage-internal-variables-svr", null ],
          [ "One class support vector machine", "svm.html#svm-usage-internal-variables-ocsvm", null ]
        ] ]
      ] ]
    ] ]
];