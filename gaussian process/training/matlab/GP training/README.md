# Training of Gaussian processes

Function   : Predict_Gp.m, ScaleVariables.m, Train_GP_and_return_hyperparameters.more

The functions have been modified from previous work that is published under a BSD 2-Clause License.

Copyright (c) 2017, Eric Bradford, Artur M. Schweidtmann and Alexei Lapkin
All rights reserved.

Last Update: 22/09/2019

To download the original unmodified algorithm please visit https://github.com/Eric-Bradford/TS-EMO
and for more information see Bradford, E., Schweidtmann, A.M. & Lapkin, A. J Glob Optim (2018) 71: 407. https://doi.org/10.1007/s10898-018-0609-2

The function are also based on the the GPML toolbox which can be obtained from http://gaussianprocess.org/gpml/code/matlab/ under the FreeBSD license.