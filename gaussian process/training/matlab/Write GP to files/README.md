# Write Gaussian processes to json / xml files

Function   : Wrties Gaussian process parameters to json or xml files that can be read by our C++ GP model.

Written by : Artur Schweidtmann, Xiaopeng Lin, Daniel Grothe, and Alexander Mitsos

Created on : 16/01/2020

Last Update: 16/01/2020
