# Gaussian process model training
The training of the Gaussian processes is based on our previous work.

For more information please see also:
https://github.com/Eric-Bradford/TS-EMO

E. Bradford, A. M. Schweidtmann, and A. A. Lapkin, Efficient multiobjective optimization employing Gaussian processes, spectral sampling and a genetic algorithm, Journal of Global Optimization, vol. 71, no. 2, pp. 407–438, 2018.








