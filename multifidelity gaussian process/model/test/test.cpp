/**********************************************************************************
* Copyright (c) 2020 Process Systems Engineering (AVT.SVT), RWTH Aachen University
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
*
* @file test.cpp
*
* @brief File implementing a test.
*
**********************************************************************************/

#include "mulfilGpData.h"
#include "mulfilGpParser.h"
#include "mulfilGp.h"

using namespace melon;


int main(int argc, char** argv)
{
	try {

		// Load a multifidelity Gaussian process from .json files
		std::cout << "Load a multifidelity Gaussian process from .json files.\n";
		MulfilGp<double> mulfilGp{ "modelData" };
		std::cout << "Loading successful.\n" << std::endl;

		// Define input
		std::vector<double> input{ -2.0, 3.0 };
		std::cout << "Consider input (-2.0, 3.0).\n";
		std::cout << "All expected results were calculated with the Python library emukit.\n" << std::endl;

		// Test low fidelity mean prediction
		std::cout << "Test low fidelity mean prediction.\n";
		std::cout << "Expected: 0.858933.\n";
		double lowMean{ mulfilGp.calculate_low_prediction_reduced_space(input) };
		std::cout << "Calculated: " << lowMean << ".\n";
		std::cout << "Error: " << abs(lowMean - 0.8589325954475839) << ".\n" << std::endl;

		// Test low fidelity variance prediction
		std::cout << "Test low fidelity variance prediction.\n";
		std::cout << "Expected: 0.131943.\n";
		double lowVar{ mulfilGp.calculate_low_variance_reduced_space(input) };
		std::cout << "Calculated: " << lowVar << ".\n";
		std::cout << "Error: " << abs(lowVar - 0.13194310842262524) << ".\n" << std::endl;

		// Test high fidelity mean prediction
		std::cout << "Test high fidelity mean prediction.\n";
		std::cout << "Expected: 1.04483.\n";
		double highMean{ mulfilGp.calculate_high_prediction_reduced_space(input) };
		std::cout << "Calculated: " << highMean << ".\n";
		std::cout << "Error: " << abs(highMean - 1.0448253526257076) << ".\n" << std::endl;

		// Test high fidelity variance prediction
		std::cout << "Test high fidelity variance prediction.\n";
		std::cout << "Expected: 0.159651.\n";
		double highVar{ mulfilGp.calculate_high_variance_reduced_space(input) };
		std::cout << "Calculated: " << highVar << ".\n";
		std::cout << "Error: " << abs(highVar - 0.1596513397161594) << ".\n" << std::endl;
	}

	catch (std::exception& e) {
		std::cerr << "\n" << "  Encountered exception:" << "\n" << e.what() << std::endl;
	}

	catch (...) {
		std::cerr << "\n" << "  Encountered an unknown fatal error. Terminating." << std::endl;
	}

	return 0;
}