from pathlib import Path
import json

import numpy as np
from sklearn.preprocessing import MinMaxScaler, StandardScaler
import GPy.kern
from emukit.model_wrappers.gpy_model_wrappers import GPyMultiOutputWrapper


def save_data_to_json(
    folderpath: Path, mf_model: GPyMultiOutputWrapper, input_scaler: MinMaxScaler, output_scalers: tuple[StandardScaler, StandardScaler]) -> None:
    
    if not mf_model.gpy_model.likelihood.likelihoods_list[0].is_fixed:
        raise Exception("mf_model.gpy_model.likelihood.likelihoods_list[0].is_fixed is not True.\n" +
                        "We currently support only noise of zero.")
    if not mf_model.gpy_model.likelihood.likelihoods_list[1].is_fixed:
        raise Exception("mf_model.gpy_model.likelihood.likelihoods_list[1].is_fixed is not True.\n" +
                        "We currently support only noise of zero.")
    if mf_model.gpy_model.likelihood.likelihoods_list[0].param_array[0] != 0.0:
        raise Exception("mf_model.gpy_model.likelihood.likelihoods_list[0].param_array[0] != 0.0.\n" +
                        "We currently support only noise of zero.")
    if mf_model.gpy_model.likelihood.likelihoods_list[1].param_array[0] != 0.0:
        raise Exception("mf_model.gpy_model.likelihood.likelihoods_list[1].param_array[0] != 0.0.\n" +
                        "We currently support only noise of zero.")
    
    folderpath.mkdir(parents = True, exist_ok = True)
    
    # Low fidelity data
    data = create_level_gp_data(0, mf_model, input_scaler, output_scalers[0])
    with (folderpath / "lowGpData.json").open("w") as outfile:
        json.dump(data, outfile, indent = 2)
        
    # High fidelity data
    data = create_level_gp_data(1, mf_model, input_scaler, output_scalers[1])
    with (folderpath / "highGpData.json").open("w") as outfile:
        json.dump(data, outfile, indent = 2)
        
    # Rho
    data = dict()
    data["rho"] = mf_model.gpy_model.kern.scaling_param[0]
    with (folderpath / "rho.json").open("w") as outfile:
        json.dump(data, outfile, indent = 2)
 
 
def load_scalers_from_json(filepath: Path) -> tuple[MinMaxScaler, StandardScaler]:
    
    with open(filepath, "r") as infile:
        data = json.load(infile)
    
    input_scaler = MinMaxScaler()
    input_scaler.scale_ = \
        (input_scaler.feature_range[1] - input_scaler.feature_range[0]) / \
            (np.array(data["problemUpperBound"]) - np.array(data["problemLowerBound"]))
    input_scaler.min_ = \
        input_scaler.feature_range[0] - np.array(data["problemLowerBound"]) * input_scaler.scale_
    
    output_scaler = StandardScaler()
    output_scaler.mean_ = np.array(data["meanOfOutput"])
    output_scaler.scale_ = np.array(data["stdOfOutput"])
    output_scaler.var_ = output_scaler.scale_**2
        
    return input_scaler, output_scaler


def load_gp_data_from_json(filepath: Path) -> tuple[np.array, np.array, np.array, np.array]:
    
    with open(filepath, "r") as infile:
        data = json.load(infile)
        
    X_scaled = np.array(data["X"])
    y_scaled = np.array(data["Y"]).reshape(-1, 1)
    K_scaled = np.array(data["K"])
    invK_scaled = np.array(data["invK"])
    
    return X_scaled, y_scaled, K_scaled, invK_scaled


def load_kernel_from_json(filepath: Path) -> GPy.kern.Kern:
    
    with open(filepath, "r") as infile:
        data = json.load(infile)
    
    input_dim = data["DX"]    
    variance = data["sf2"]
    lengthscale = np.array(data["ell"])
    kernel_type = int_to_kernel_type(data["matern"])
    
    return kernel_type(input_dim, variance, lengthscale, True)
    

def create_level_gp_data(
    level: int, mf_model: GPyMultiOutputWrapper, input_scaler: MinMaxScaler, output_scaler: StandardScaler) -> dict:
    
    data = dict()
    
    mask = mf_model.X[:, -1] == level
    X = mf_model.X[mask, :-1]
    y = mf_model.Y[mask]
    X_scaled = input_scaler.transform(X)
    y_scaled = output_scaler.transform(y)
    
    data["nX"] = X.shape[0]
    data["DX"] = X.shape[1]
    data["DY"] = 1
    
    data["X"] = X_scaled.tolist()
    data["Y"] = y_scaled.flatten().tolist()
    
    kernel = mf_model.gpy_model.kern.kernels[level]
    noise = mf_model.gpy_model.likelihood.likelihoods_list[level].variance
    if (noise.size == 1):
        noise = noise * np.ones((X.shape[0],))
    
    K = (kernel.K(X, X) + np.diag(noise)) / output_scaler.var_.item()
    K_inv = np.linalg.inv(K)
    data["K"] = K.tolist()
    data["invK"] = K_inv.tolist()
    
    data["meanfunction"] = output_scaler.transform([[0]]).item()
    data["matern"] = kernel_type_to_int(kernel)
    
    data["sf2"] = kernel.variance[0] / output_scaler.var_.item()
    data["ell"] = (input_scaler.scale_ * kernel.lengthscale).tolist()
    if (len(data["ell"]) == 1):
        data["ell"] = data["ell"] * X.shape[1]
    
    data["problemLowerBound"] = input_scaler.data_min_.tolist()
    data["problemUpperBound"] = input_scaler.data_max_.tolist()
    data["stdOfOutput"] = output_scaler.scale_.item()
    data["meanOfOutput"] = output_scaler.mean_.item()
    
    return data
    

def kernel_type_to_int(kernel: GPy.kern.Kern) -> int:
    
    if (isinstance(kernel, GPy.kern.RBF) or isinstance(kernel, GPy.kern.ExpQuad)): return 999
    if isinstance(kernel, GPy.kern.OU): return 1
    if isinstance(kernel, GPy.kern.Matern32): return 3
    if isinstance(kernel, GPy.kern.Matern52): return 5
    raise Exception(f"The given kernel ({type(kernel).__name__}) is not supported.")
    
    
def int_to_kernel_type(type: int) -> GPy.kern.Kern:
    
    if (type == 999): return GPy.kern.ExpQuad
    if (type == 1): return GPy.kern.OU
    if (type == 3): return GPy.kern.Matern32
    if (type == 5): return GPy.kern.Matern52
    raise Exception(f"The given type number ({type}) is not known.")
    